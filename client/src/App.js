import React, { Component } from 'react';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import store from './store';
import classnames from 'classnames';
import Loadable from 'react-loadable';

// utils components
import LoadingSpinner from './components/common/LoadingSpinner';
import SuccessMessage from './components/common/SuccessMessage';

import Home from './components/Home';

import PrivateRoute from './components/common/PrivateRoute';
import AdminPrivateRoute from './components/common/AdminPrivateRoute';

// admin components
import AdminNavbar from './components/admin/layoutAdmin/Navbar';
// import AdminFooter from './components/admin/layoutAdmin/Footer';
import AdminLogin from './components/admin/auth/Login';

//non-auth components routes

// user components

// styling
import './assets/css/App.css';
import './assets/flaticons/flaticons.css';
import PageNotFound from './components/common/PageNotFound';

//admin components routes - with react-loadable code splitting
const AdminDashboard = Loadable({
  loader: () => import('./components/admin/dashboard/Dashboard'),
  loading: LoadingSpinner
});
const AdminAbout = Loadable({
  loader: () => import('./routes/admin/about/IndexAbout'),
  loading: LoadingSpinner
});
const AdminPosts = Loadable({
  loader: () => import('./routes/admin/posts/IndexPost'),
  loading: LoadingSpinner
});
const AdminPost = Loadable({
  loader: () => import('./routes/admin/posts/SinglePost'),
  loading: LoadingSpinner
});
const AdminEditPost = Loadable({
  loader: () => import('./routes/admin/posts/EditPost'),
  loading: LoadingSpinner
});
const AdminCreatePost = Loadable({
  loader: () => import('./routes/admin/posts/CreatePost'),
  loading: LoadingSpinner
});
const AdminCategories = Loadable({
  loader: () => import('./routes/admin/categories/IndexCategory'),
  loading: LoadingSpinner
});
const AdminSocialMedia = Loadable({
  loader: () => import('./routes/admin/social-media/IndexSocialMedia'),
  loading: LoadingSpinner
});
const AdminSettings = Loadable({
  loader: () => import('./routes/admin/settings/IndexSettings'),
  loading: LoadingSpinner
});
const AdminSchedule = Loadable({
  loader: () => import('./routes/admin/schedule/IndexSchedule'),
  loading: LoadingSpinner
});
const AdminEnquiry = Loadable({
  loader: () => import('./routes/admin/enquiry/IndexEnquiry'),
  loading: LoadingSpinner
});
const AdminTestimony = Loadable({
  loader: () => import('./routes/admin/testimony/IndexTestimony'),
  loading: LoadingSpinner
});
const AdminSingleTestimony = Loadable({
  loader: () => import('./routes/admin/testimony/SingleTestimony'),
  loading: LoadingSpinner
});
const AdminCreateTestimony = Loadable({
  loader: () => import('./routes/admin/testimony/CreateTestimony'),
  loading: LoadingSpinner
});

//check for token
if (localStorage.jwtToken) {
  // set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // check for expired tokens
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    //logoutUser
    store.dispatch(logoutUser());
    // clear current profile
    // store.dispatch(clearCurrentProfile());
    //redirect to login
    window.location.href = 'login';
  }
}

class App extends Component {
  state = {
    menuIsOpen: true
  };

  onClickMenuToggle = () => {
    this.setState(prevState => ({
      menuIsOpen: !prevState.menuIsOpen
    }));
  };

  render() {
    const { isLoading, success, auth } = this.props;
    const { menuIsOpen } = this.state;
    const hamburgerNav = (
      <div className="top-nav">
        <div className="show-hide-nav" onClick={this.onClickMenuToggle}>
          <div className="hamburger-line" />
          <div className="hamburger-line" />
          <div className="hamburger-line" />
          <div className="hamburger-line" />
        </div>
      </div>
    );
    return (
      <div className="App">
        <BrowserRouter>
          <>
            {isLoading ? <LoadingSpinner /> : null}
            <AdminNavbar menuIsOpen={menuIsOpen} />
            <Route path="/login" component={AdminLogin} />
            <div
              className={classnames('main-content ml-auto nopadding ', {
                'col-md-10': menuIsOpen,
                'col-md-12': !menuIsOpen
              })}
            >
              {auth.isAuthenticated ? hamburgerNav : null}
              <div className="container">
                {success ? <SuccessMessage success={success} /> : null}

                <Switch>
                  <Route exact path="/admin" component={Home} />
                  <PrivateRoute
                    path="/admin/dashboard"
                    component={AdminDashboard}
                  />
                  <AdminPrivateRoute
                    path="/admin/about"
                    component={AdminAbout}
                  />
                  <PrivateRoute
                    exact
                    path="/admin/posts"
                    component={AdminPosts}
                  />
                  <PrivateRoute
                    path="/admin/posts/:slug"
                    component={AdminPost}
                  />
                  <PrivateRoute
                    path="/admin/create-post"
                    component={AdminCreatePost}
                  />
                  <PrivateRoute
                    path="/admin/edit-post/:slug"
                    component={AdminEditPost}
                  />
                  <AdminPrivateRoute
                    path="/admin/social-media"
                    component={AdminSocialMedia}
                  />
                  <AdminPrivateRoute
                    path="/admin/schedule"
                    component={AdminSchedule}
                  />
                  <AdminPrivateRoute
                    path="/admin/enquiry"
                    component={AdminEnquiry}
                  />
                  <PrivateRoute
                    path="/admin/category"
                    component={AdminCategories}
                  />
                  <AdminPrivateRoute
                    exact
                    path="/admin/testimony"
                    component={AdminTestimony}
                  />
                  <AdminPrivateRoute
                    path="/admin/testimony/:testimony_id"
                    component={AdminSingleTestimony}
                  />
                  <AdminPrivateRoute
                    path="/admin/create-testimony"
                    component={AdminCreateTestimony}
                  />
                  <PrivateRoute
                    path="/admin/settings"
                    component={AdminSettings}
                  />
                  <Route component={PageNotFound} />
                </Switch>
              </div>
            </div>
            {/* <AdminFooter /> */}
          </>
        </BrowserRouter>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  isLoading: state.isLoading,
  success: state.success
});

export default connect(mapStateToProps)(App);
