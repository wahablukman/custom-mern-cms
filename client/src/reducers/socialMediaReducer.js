import {
  GET_SOCIAL_MEDIA,
  CREATE_OR_UPDATE_SOCIAL_MEDIA
} from '../actions/types';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_SOCIAL_MEDIA:
      return action.payload;
    case CREATE_OR_UPDATE_SOCIAL_MEDIA:
      return action.payload;
    default:
      return state;
  }
};
