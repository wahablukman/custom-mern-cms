import {
  GET_ALL_CATEGORIES,
  DELETE_CATEGORY,
  CREATE_CATEGORY
} from '../actions/types';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_CATEGORIES:
      return action.payload;
    case CREATE_CATEGORY:
      return [...state, action.payload];
    case DELETE_CATEGORY:
      return state.filter(category => category._id !== action.payload);
    default:
      return state;
  }
};
