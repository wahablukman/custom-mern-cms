import {
  GET_MAIN_PROFILE,
  CREATE_OR_UPDATE_MAIN_PROFILE
} from '../actions/types';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_MAIN_PROFILE:
      return action.payload;
    case CREATE_OR_UPDATE_MAIN_PROFILE:
      return action.payload;
    default:
      return state;
  }
};
