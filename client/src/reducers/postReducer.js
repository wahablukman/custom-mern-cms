import {
  GET_ALL_POSTS,
  GET_POST,
  ADD_POST,
  EDIT_POST,
  DELETE_POST
} from '../actions/types';

const initialState = {
  posts: [],
  post: {},
  pagination: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_POSTS:
      return {
        ...state,
        posts: action.payload.docs,
        pagination: {
          totalDocs: action.payload.totalDocs,
          limit: action.payload.limit,
          hasPrevPage: action.payload.hasPrevPage,
          hasNextPage: action.payload.hasNextPage,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          prevPage: action.payload.prevPage,
          nextPage: action.payload.nextPage
        }
      };
    case GET_POST:
      return {
        ...state,
        post: action.payload
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.payload, state.posts]
      };
    case EDIT_POST:
      return {
        ...state,
        posts: state.posts.map(
          post => (post._id === action.payload._id ? action.payload : post)
        ),
        post: action.payload
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post._id !== action.payload)
      };
    default:
      return state;
  }
};
