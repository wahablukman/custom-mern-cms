import { GET_ABOUT, CREATE_OR_UPDATE_ABOUT } from '../actions/types';

const initialState = '';

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ABOUT:
      return action.payload;
    case CREATE_OR_UPDATE_ABOUT:
      return action.payload;
    default:
      return state;
  }
};
