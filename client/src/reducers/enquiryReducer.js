import {
  GET_ALL_ENQUIRIES,
  GET_ENQUIRY,
  DELETE_ENQUIRY
} from '../actions/types';

const initialState = {
  enquiries: [],
  enquiry: {},
  pagination: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_ENQUIRIES:
      return {
        ...state,
        enquiries: action.payload.docs,
        pagination: {
          totalDocs: action.payload.totalDocs,
          limit: action.payload.limit,
          hasPrevPage: action.payload.hasPrevPage,
          hasNextPage: action.payload.hasNextPage,
          page: action.payload.page,
          totalPages: action.payload.totalPages,
          prevPage: action.payload.prevPage,
          nextPage: action.payload.nextPage
        }
      };
    case GET_ENQUIRY:
      return {
        ...state,
        enquiry: action.payload
      };
    case DELETE_ENQUIRY:
      return {
        ...state,
        enquiries: state.enquiries.filter(
          enquiry => enquiry._id !== action.payload
        )
      };
    default:
      return state;
  }
};
