import {
  GET_ALL_TESTIMONIES,
  GET_TESTIMONY,
  DELETE_TESTIMONY,
  CREATE_TESTIMONY,
  UPDATE_TESTIMONY
} from '../actions/types';

const initialState = {
  testimonies: [],
  testimony: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_TESTIMONY:
      return {
        ...state,
        testimonies: [action.payload, state.testimonies]
      };
    case UPDATE_TESTIMONY:
      return {
        ...state,
        testimonies: state.testimonies.map(testimony =>
          action.payload._id ? action.payload : testimony
        )
      };
    case GET_ALL_TESTIMONIES:
      return {
        ...state,
        testimonies: action.payload
      };
    case GET_TESTIMONY:
      return {
        ...state,
        testimony: action.payload
      };
    case DELETE_TESTIMONY:
      return {
        ...state,
        testimonies: state.testimonies.filter(
          testimony => testimony._id !== action.payload
        )
      };
    default:
      return state;
  }
};
