import { combineReducers } from 'redux';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import successReducer from './successReducer';
import postReducer from './postReducer';
import aboutReducer from './aboutReducer';
import socialMediaReducer from './socialMediaReducer';
import loadingReducer from './loadingReducer';
import categoryReducer from './categoryReducer';
import mainProfileReducer from './mainProfileReducer';
import enquiryReducer from './enquiryReducer';
import testimonyReducer from './testimonyReducer';

export default combineReducers({
  auth: authReducer,
  posts: postReducer,
  mainProfile: mainProfileReducer,
  categories: categoryReducer,
  enquiries: enquiryReducer,
  testimonies: testimonyReducer,
  about: aboutReducer,
  socialMedia: socialMediaReducer,
  errors: errorReducer,
  success: successReducer,
  isLoading: loadingReducer
});
