import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, NavLink } from 'react-router-dom';
import * as authActions from '../../../actions/authActions';
import * as mainProfileActions from '../../../actions/mainProfileActions';
// import logo from '../../../assets/images/logo.png';
import classnames from 'classnames';
import isEmpty from '../../../validation/isEmpty';

class Navbar extends Component {
  state = {
    logo: ''
  };

  componentDidMount() {
    const { getMainProfile } = this.props;
    getMainProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps.mainProfile.logo)) {
      this.setState({
        logo: nextProps.mainProfile.logo
      });
    }
  }

  onClickLogout = e => {
    const { logoutUser, history } = this.props;
    logoutUser();
    history.push('/login');
  };

  render() {
    const { isAuthenticated, user } = this.props.auth;
    const { menuIsOpen } = this.props;
    const authLinks = (
      <React.Fragment>
        <li>
          <NavLink exact to="/admin/dashboard">
            <i className="fas fa-chart-pie" />
            <span className="text">Dashboard</span>
          </NavLink>
        </li>
        {user.role === 'Admin' ? (
          <li>
            <NavLink exact to="/admin/about">
              <i className="fas fa-info" />
              <span className="text">About</span>
            </NavLink>
          </li>
        ) : null}
        <li>
          <NavLink exact to="/admin/posts">
            <i className="fas fa-file-signature" />
            <span className="text">Posts</span>
          </NavLink>
        </li>
        {user.role === 'Admin' ? (
          <li>
            <NavLink exact to="/admin/social-media">
              <i className="fab fa-twitter" />
              <span className="text">Social Media</span>
            </NavLink>
          </li>
        ) : null}
        <li>
          <NavLink exact to="/admin/category">
            <i className="fas fa-tags" />
            <span className="text">Categories</span>
          </NavLink>
        </li>
        {user.role === 'Admin' ? (
          <li>
            <NavLink exact to="/admin/testimony">
              <i className="fas fa-comments" />
              <span className="text">Testimony</span>
            </NavLink>
          </li>
        ) : null}
        {user.role === 'Admin' ? (
          <li>
            <NavLink exact to="/admin/schedule">
              <i className="fas fa-calendar" />
              <span className="text">Schedule</span>
            </NavLink>
          </li>
        ) : null}
        {user.role === 'Admin' ? (
          <li>
            <NavLink exact to="/admin/enquiry">
              <i className="fas fa-envelope" />
              <span className="text">Enquiry</span>
            </NavLink>
          </li>
        ) : null}
        <hr />
        <li>
          <NavLink to="/admin/settings">
            <i className="fas fa-cog" />
            <span className="text">Settings</span>
          </NavLink>
        </li>
        <hr />
        <br />
        <li>
          {/* <span className="icon flaticon-logout" /> */}
          {/* eslint-disable-next-line */}
          <a href="#" onClick={this.onClickLogout} className="text">
            <i className="fas fa-door-open" />
            <span className="text">Logout</span>
          </a>
        </li>
      </React.Fragment>
    );
    const guestLinks = (
      <React.Fragment>
        <li>
          <NavLink to="/login">Login</NavLink>
        </li>
      </React.Fragment>
    );
    const wholeNav = (
      <nav
        className={classnames(
          'navbar navbar-expand-lg navbar-light bg-light flex-parent col-md-2',
          {
            'push-left-out-of-the-way': !menuIsOpen
          }
        )}
      >
        <div className="navbar-brand logo-text text-center">
          <img src={this.state.logo} alt="Main profile's logo" />
        </div>
        <hr />
        <div id="navbarNav">
          <ul className="flex-parent nav-custom-ul">
            {isAuthenticated ? authLinks : guestLinks}
          </ul>
        </div>
      </nav>
    );
    return <React.Fragment>{isAuthenticated ? wholeNav : null}</React.Fragment>;
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  mainProfile: state.mainProfile
});

const mapDispatchToProps = {
  ...authActions,
  ...mainProfileActions
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Navbar)
);
