import React, { Component } from 'react';
class Footer extends Component {
  state = {};
  render() {
    return (
      <footer>
        <small>© DeftByte - {new Date().getFullYear()}</small>
      </footer>
    );
  }
}

export default Footer;
