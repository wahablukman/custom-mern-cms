import React, { Component } from 'react';
import Moment from 'react-moment';

class EnquiryItem extends Component {
  state = {};
  render() {
    const { enquiry, onClickDelete, onClickShowEnquiry } = this.props;
    return (
      <div className="flex-parent single-enquiry-list table-list card-custom">
        <div className="message">
          {/* eslint-disable-next-line */}
          <a onClick={onClickShowEnquiry}>{enquiry.message}</a>
        </div>
        <div className="name">
          <p>{enquiry.name}</p>
        </div>
        <div className="email">{enquiry.email}</div>
        <div className="date-created">
          <Moment fromNow>{enquiry.createdAt}</Moment>
        </div>
        <div className="btn-group action-btn" role="group">
          <button
            id="enquiry-action-dropdown"
            type="button"
            className="btn btn-outline-secondary dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Action
          </button>
          <div
            className="dropdown-menu"
            aria-labelledby="enquiry-action-dropdown"
          >
            {/* eslint-disable-next-line */}
            <a onClick={onClickShowEnquiry}>View</a>
            <hr />
            {/* eslint-disable-next-line */}
            <a onClick={onClickDelete}>Delete</a>
          </div>
        </div>
      </div>
    );
  }
}

export default EnquiryItem;
