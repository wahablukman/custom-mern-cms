import React, { Component } from 'react';

class EnquiryModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { enquiry, onClickCloseModal } = this.props;
    return (
      <>
        <h1>{enquiry.name}</h1>
        <h4>{enquiry.email}</h4>
        {enquiry.phone ? <h4> {enquiry.phone} </h4> : null}
        <hr />
        <p>{enquiry.message}</p>
        <button
          className="btn btn-outline-secondary close-button-modal"
          onClick={onClickCloseModal}
        >
          Close
        </button>
      </>
    );
  }
}

export default EnquiryModal;
