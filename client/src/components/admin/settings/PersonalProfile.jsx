import React, { Component } from 'react';
import TextFieldGroup from '../../common/TextFieldGroup';
import FileUpload from '../../common/FileUpload';

class PersonalProfile extends Component {
  state = {};
  render() {
    const { avatar, name, role, onDropChange, onSubmit, onChange } = this.props;
    return (
      <>
        {avatar ? (
          <>
            <div className="avatar-preview">
              <img src={avatar} alt={`${name}'s avatar`} />
            </div>
            <br />
          </>
        ) : null}
        <form onSubmit={onSubmit}>
          <FileUpload
            label={`Profile Picture`}
            accept="image/*"
            name="image"
            multiple={false}
            onDrop={onDropChange}
          />
          <br />
          <TextFieldGroup
            label="name"
            name="name"
            onChange={onChange}
            value={name}
          />
          <TextFieldGroup
            label="Role"
            name="role"
            onChange={onChange}
            value={role}
            // disabled={`disabled`}
          />
          <br />
          <button type="submit" className="btn btn-primary">
            <i className="fas fa-paper-plane" />
            Update Settings
          </button>
        </form>
      </>
    );
  }
}

export default PersonalProfile;
