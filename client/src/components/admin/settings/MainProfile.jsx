import React, { Component } from 'react';
import { connect } from 'react-redux';
import TextFieldGroup from '../../common/TextFieldGroup';
import isEmpty from '../../../validation/isEmpty';
import * as mainProfileActions from '../../../actions/mainProfileActions';
import FileUpload from '../../common/FileUpload';

class MainProfile extends Component {
  state = {
    name: '',
    email: '',
    logo: '',
    logoPreview: '',
    newLogo: {}
  };

  componentDidMount() {
    const { getMainProfile } = this.props;
    getMainProfile();
  }

  componentWillReceiveProps(nextProps) {
    if (!isEmpty(nextProps.mainProfile)) {
      this.setState({
        name: nextProps.mainProfile.name,
        email: nextProps.mainProfile.email,
        logo: nextProps.mainProfile.logo
      });
    }
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onDropChange = file => {
    this.setState(
      {
        newLogo: file[0]
      },
      () => {
        this.handleImagePreview(this.state.newLogo);
      }
    );
  };

  handleImagePreview = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          logo: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  onSubmit = e => {
    const { createOrUpdateMainProfile, mainProfile } = this.props;
    e.preventDefault();
    let profileData = new FormData();
    profileData.append('name', this.state.name);
    profileData.append('email', this.state.email);

    if (mainProfile.logo !== this.state.logo) {
      profileData.append('logo', this.state.newLogo);
    }

    return createOrUpdateMainProfile(profileData);
  };

  render() {
    const { name, email, logo } = this.state;
    return (
      <>
        {logo ? (
          <>
            <div className="avatar-preview">
              <img src={logo} alt={`${name}'s logo`} />
            </div>
            <br />
          </>
        ) : null}
        <form onSubmit={this.onSubmit}>
          <FileUpload
            label={`Profile Picture`}
            accept="image/*"
            name="logo"
            multiple={false}
            onDrop={this.onDropChange}
          />
          <br />
          <TextFieldGroup
            label="name"
            name="name"
            onChange={this.onChange}
            value={name}
          />
          <TextFieldGroup
            label="Email"
            name="email"
            onChange={this.onChange}
            value={email}
            // disabled={`disabled`}
          />
          <br />
          <button type="submit" className="btn btn-primary">
            <i className="fas fa-paper-plane" />
            Update Settings
          </button>
        </form>
      </>
    );
  }
}

const mapStateToProps = state => ({
  mainProfile: state.mainProfile
});

const mapDispatchToProps = {
  ...mainProfileActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainProfile);
