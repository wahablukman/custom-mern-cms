import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TextFieldGroup from '../../common/TextFieldGroup';
import * as authActions from '../../../actions/authActions';

class Login extends Component {
  state = {
    email: '',
    password: ''
  };

  componentDidMount() {
    document.title = 'Admin Login';
    const { history, auth } = this.props;
    if (auth.isAuthenticated) {
      history.push('/dashboard');
    }
  }

  onSubmit = e => {
    e.preventDefault();
    const { loginUser, history } = this.props;
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    loginUser(userData, history);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const { email, password } = this.state;
    const { errors } = this.props;
    // console.log(this.props);
    return (
      <div className="col-md-4 login-page m-auto">
        <h1 className="text-center">Login Admin</h1>
        <form noValidate onSubmit={this.onSubmit}>
          <TextFieldGroup
            name="email"
            placeholder="Type Your Registered Email"
            value={email}
            onChange={this.onChange}
            type="email"
            error={errors.email}
          />
          <TextFieldGroup
            name="password"
            placeholder="Your Password"
            value={password}
            onChange={this.onChange}
            type="password"
            error={errors.password}
          />
          <br />
          <button type="submit" className="btn btn-primary m-auto text-center">
            Login
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  success: state.success
});

export default connect(
  mapStateToProps,
  authActions
)(withRouter(Login));
