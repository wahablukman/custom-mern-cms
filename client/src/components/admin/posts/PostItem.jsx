import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import isEmpty from '../../../validation/isEmpty';
import Moment from 'react-moment';
import CategoryItem from './CategoryItem';

class PostItem extends Component {
  state = {};
  render() {
    const { post, onDelete } = this.props;
    // return each post content
    let category;

    //set category
    if (!isEmpty(post.categories)) {
      // eslint-disable-next-line
      category = post.categories.map((category, index) => {
        if (!isEmpty(category)) {
          return <CategoryItem category={category} key={index} />;
        }
      });
    }

    return (
      <div className="flex-parent single-post-list table-list card-custom">
        <div className="title">
          <Link to={`/admin/edit-post/${post.slug}`}>{post.title}</Link>
        </div>
        <div className="author">
          <p>{post.user ? post.user.name : null}</p>
        </div>
        <div className="categories">{category}</div>
        <div className="date-created">
          <Moment fromNow>{post.createdAt}</Moment>
        </div>
        <div className="btn-group action-btn" role="group">
          <button
            id="post-action-dropdown"
            type="button"
            className="btn btn-outline-secondary dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Action
          </button>
          <div className="dropdown-menu" aria-labelledby="post-action-dropdown">
            <Link to={`/admin/edit-post/${post.slug}`}>Edit</Link>
            <hr />
            {/* eslint-disable-next-line */}
            <a href="#" onClick={onDelete}>
              Delete
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default PostItem;
