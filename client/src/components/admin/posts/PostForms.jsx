import React, { Component } from 'react';
import TextFieldGroup from '../../common/TextFieldGroup';
import FileUpload from '../../common/FileUpload';
import WYSIWYG from '../../common/WYSIWYG';
import MultipleSelectInput from '../../common/MultipleSelectInput';

class PostForms extends Component {
  state = {};
  render() {
    const {
      onSubmit,
      onDropChange,
      title,
      onChange,
      errors,
      content,
      modules,
      onWYSIWYGChange,
      categories,
      categoryOptions,
      image,
      selectCategoryOption,
      imagePreview,
      submitButtonName,
      titleName
    } = this.props;
    return (
      <>
        <h1>{titleName}</h1>
        <br />
        <form onSubmit={onSubmit} encType="multipart/form-data">
          <div className="container nopadding">
            <div className="row">
              <div className="col-md-8 nopadding">
                <FileUpload
                  label={`Featured Image`}
                  accept="image/*"
                  name="image"
                  multiple={false}
                  onDrop={onDropChange}
                />
                <br />
                <TextFieldGroup
                  label="Title"
                  type="text"
                  name="title"
                  value={title}
                  onChange={onChange}
                  placeholder="Your post title"
                  error={errors.title}
                />
                <label>Content</label>
                <WYSIWYG
                  value={content}
                  modules={modules}
                  onChange={onWYSIWYGChange}
                  error={errors.content}
                />
                <br />
              </div>
              <div className="col-md-4 ">
                <label>Categories</label>
                <MultipleSelectInput
                  value={categories}
                  options={categoryOptions}
                  onChange={selectCategoryOption}
                  open={false}
                  multiple={true}
                  includeSelectAll={true}
                  includeFilter={true}
                  dropdownHeight={200}
                  allSelectedPlaceholder={'All categories are selected'}
                  placeholder={"Choose post's categories"}
                />
                <br />
                <label>Featured Image Preview</label>
                {image !== '' ? (
                  <img src={imagePreview} alt="" />
                ) : (
                  <p className="still-empty">
                    Currently does not have featured image
                  </p>
                )}
              </div>
            </div>
          </div>
          <button className="btn btn-primary" type="submit">
            <i className="fas fa-paper-plane" />
            {submitButtonName}
          </button>
        </form>
      </>
    );
  }
}

export default PostForms;
