import React, { Component } from 'react';

class CategoryItem extends Component {
  state = {};
  render() {
    const { category } = this.props;
    return <p className="category d-inline-block">{category}</p>;
  }
}

export default CategoryItem;
