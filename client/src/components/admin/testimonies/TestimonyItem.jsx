import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

class TestimonyItem extends Component {
  state = {};
  render() {
    const { testimony } = this.props;
    return (
      <div className="flex-parent single-testimony-list table-list card-custom">
        <div className="testimony">
          <Link to={`/admin/testimony/${testimony._id}`}>
            {testimony.testimony}
          </Link>
        </div>
        <div className="name">
          <p>{testimony.name}</p>
        </div>
        <div className="avatar">
          <img src={testimony.avatar} alt={`${testimony.name}'s avatar`} />
        </div>
        <div className="date-created">
          <Moment fromNow>{testimony.createdAt}</Moment>
        </div>
        <div className="btn-group action-btn" role="group">
          <button
            id="testimony-action-dropdown"
            type="button"
            className="btn btn-outline-secondary dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Action
          </button>
          <div
            className="dropdown-menu"
            aria-labelledby="testimony-action-dropdown"
          >
            <Link to={`/admin/testimony/${testimony._id}`}>Edit</Link>
            <hr />
            {/* eslint-disable-next-line */}
            <a
            // onClick={onDelete}
            >
              Delete
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default TestimonyItem;
