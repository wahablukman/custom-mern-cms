import React, { Component } from 'react';
import FileUpload from '../../../components/common/FileUpload';
import TextFieldGroup from '../../../components/common/TextFieldGroup';

class TestimonyForms extends Component {
  state = {};
  render() {
    const {
      titleName,
      onSubmit,
      onDropChange,
      onChange,
      name,
      testimony,
      submitButtonName,
      avatar,
      avatarPreview
    } = this.props;
    return (
      <>
        <h1>{titleName}</h1>
        <br />
        <form onSubmit={onSubmit} encType="multipart/form-data">
          {avatar !== '' ? (
            <>
              <label>Profile Picture Preview</label>
              <img src={avatarPreview} alt="" />
              <br />
              <br />
            </>
          ) : null}
          <FileUpload
            label={`Testimoner's profile picture`}
            accept="image/*"
            name="avatar"
            multiple={false}
            onDrop={onDropChange}
          />
          <br />
          <TextFieldGroup
            label="Name"
            type="text"
            name="name"
            value={name}
            onChange={onChange}
            placeholder="Testimoner's name"
          />
          <TextFieldGroup
            label="Testimony"
            type="text"
            name="testimony"
            value={testimony}
            onChange={onChange}
            placeholder="Testimony"
          />
          <br />
          <button className="btn btn-primary" type="submit">
            <i className="fas fa-paper-plane" />
            {submitButtonName}
          </button>
        </form>
      </>
    );
  }
}

export default TestimonyForms;
