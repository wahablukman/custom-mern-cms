import React, { Component } from 'react';
import classnames from 'classnames';

class Pagination extends Component {
  state = {};
  render() {
    const { currentPage, index, onClick } = this.props;
    return (
      <li
        className={classnames('page-item', {
          active: currentPage === index
        })}
      >
        {/* eslint-disable-next-line */}
        <a className="page-link" onClick={onClick}>
          {index}
        </a>
      </li>
    );
  }
}

export default Pagination;
