import React, { Component } from 'react';
import loadingSpinner from '../../assets/images/loading.svg';

class LoadingSpinner extends Component {
  state = {};
  render() {
    return (
      <div className="loading-spinner">
        <img src={loadingSpinner} alt="" />
      </div>
    );
  }
}

export default LoadingSpinner;
