import React, { Component } from 'react';
import classnames from 'classnames';

class SelectListGroup extends Component {
  state = {};
  render() {
    const { name, value, onChange, error, info, options, label } = this.props;

    const selectOptions = options.map(option => (
      <option key={option.label} value={option.value}>
        {option.label}
      </option>
    ));

    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="select-custom">
          <select
            className={classnames('form-control', {
              'is-invalid': error
            })}
            name={name}
            value={value}
            onChange={onChange}
          >
            {selectOptions}
          </select>
          <i className="fas fa-caret-down" />
        </div>
        {info && <small className="form-text text-muted">{info}</small>}
        {error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }
}

export default SelectListGroup;
