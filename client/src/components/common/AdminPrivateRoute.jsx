import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={
      props => {
        if (auth.isAuthenticated === true && auth.user.role === 'Admin') {
          return <Component {...props} />;
        } else if (
          auth.isAuthenticated === true &&
          auth.user.role !== 'Admin'
        ) {
          return <Redirect to="/unauthorized" />;
        } else if (
          auth.isAuthenticated === false &&
          auth.user.role !== 'Admin'
        ) {
          return <Redirect to="/login" />;
        }
      }
      // auth.isAuthenticated === true ? (
      //   <Component {...props} />
      // ) : (
      //   <Redirect to="/admin/login" />
      // )
    }
  />
);

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(PrivateRoute);
