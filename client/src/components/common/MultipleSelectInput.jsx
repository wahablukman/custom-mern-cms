import React, { Component } from 'react';
import Picky from 'react-picky';
import 'react-picky/dist/picky.css'; // Include CSS

class MultipleSelectInput extends Component {
  state = {};
  render() {
    const {
      value,
      options,
      onChange,
      open,
      multiple,
      includeSelectAll,
      dropdownHeight,
      placeholder,
      allSelectedPlaceholder,
      includeFilter
    } = this.props;
    return (
      <div className="form-group">
        <Picky
          value={value}
          options={options}
          onChange={onChange}
          open={open}
          multiple={multiple}
          includeSelectAll={includeSelectAll}
          includeFilter={includeFilter}
          dropdownHeight={dropdownHeight}
          allSelectedPlaceholder={allSelectedPlaceholder}
          placeholder={placeholder}
        />
      </div>
    );
  }
}

export default MultipleSelectInput;
