import React, { Component } from 'react';

class PageNotFound extends Component {
  state = {};
  render() {
    return (
      <div className="d-flex">
        <h1>Error: 404 - Page not Found</h1>
      </div>
    );
  }
}

export default PageNotFound;
