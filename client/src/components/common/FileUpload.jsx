import React, { Component } from 'react';
import Dropzone from 'react-dropzone';

class FileUpload extends Component {
  state = {};
  render() {
    const { label, accept, name, multiple, onDrop } = this.props;
    return (
      <>
        <label>{label}</label>
        <div className="dropzone">
          <Dropzone
            accept={accept}
            name={name}
            multiple={multiple}
            onDrop={onDrop}
          >
            <i className="fas fa-cloud-upload-alt" />
            <h5>Click here / drag your image here</h5>
          </Dropzone>
        </div>
      </>
    );
  }
}

export default FileUpload;
