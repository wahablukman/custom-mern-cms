import React, { Component } from 'react';

class SuccessMessage extends Component {
  state = {};
  render() {
    const { success } = this.props;
    return (
      <React.Fragment>
        {/* <div className="success-message">
         <p>{success}</p>
       </div> */}
        <div className="alert alert-success" role="alert">
          <h4 className="alert-heading">Success!</h4>
          <p className="mb-0">{success}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default SuccessMessage;
