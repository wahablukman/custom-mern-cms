import React, { Component } from 'react';
import classnames from 'classnames';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

class WYSIWYG extends Component {
  state = {};
  render() {
    const { value, onChange, formats, modules, error } = this.props;
    return (
      <div className="form-group">
        <ReactQuill
          value={value}
          modules={modules}
          formats={formats}
          onChange={onChange}
          className={classnames({
            'is-invalid': error
          })}
        />
        {error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }
}

export default WYSIWYG;
