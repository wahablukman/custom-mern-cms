import React, { Component } from 'react';
import classnames from 'classnames';

class TextFieldGroup extends Component {
  state = {};
  render() {
    const {
      name,
      type,
      placeholder,
      value,
      onChange,
      disabled,
      error,
      info,
      hasPrepend,
      label
    } = this.props;

    const prepend = (
      <div className="input-group-prepend">
        <div className="input-group-text">
          <i className={`fab fa-${hasPrepend}`} />
        </div>
      </div>
    );
    return (
      <>
        <label>{label}</label>
        <div className="input-group form-group">
          {hasPrepend ? prepend : null}
          <input
            type={type}
            className={classnames('form-control', {
              'is-invalid': error
            })}
            placeholder={placeholder}
            name={name}
            value={value}
            onChange={onChange}
            disabled={disabled}
          />
          {info && <small className="form-text text-muted">{info}</small>}
          {error && <div className="invalid-feedback">{error}</div>}
        </div>
      </>
    );
  }
}

TextFieldGroup.defaultProps = {
  type: 'text',
  hasPrepend: false
};

export default TextFieldGroup;
