import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as testimonyActions from '../../../actions/testimonyActions';
import TestimonyForms from '../../../components/admin/testimonies/TestimonyForms';

class CreateTestimony extends Component {
  state = {
    avatar: '',
    avatarPreview: '',
    name: '',
    testimony: ''
  };

  onDropChange = file => {
    this.setState(
      {
        avatar: file[0]
      },
      () => {
        this.handleImagePreview(this.state.avatar);
      }
    );
  };

  handleImagePreview = avatar => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          avatarPreview: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(avatar);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { createTestimony, history } = this.props;
    let testimonyData = new FormData();
    testimonyData.append('avatar', this.state.avatar);
    testimonyData.append('name', this.state.name);
    testimonyData.append('testimony', this.state.testimony);
    createTestimony(testimonyData, history);
  };

  render() {
    const { name, testimony, avatar, avatarPreview } = this.state;
    return (
      <div className="col-md-6">
        <TestimonyForms
          name={name}
          testimony={testimony}
          avatar={avatar}
          avatarPreview={avatarPreview}
          onDropChange={this.onDropChange}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          titleName={`Create Testimony`}
          submitButtonName={`Create Testimony`}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  ...testimonyActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateTestimony);
