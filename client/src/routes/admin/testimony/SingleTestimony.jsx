import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as testimonyActions from '../../../actions/testimonyActions';
import TestimonyForms from '../../../components/admin/testimonies/TestimonyForms';

class SingleTestimony extends Component {
  state = {
    avatar: '',
    avatarPreview: '',
    name: '',
    testimony: ''
  };

  componentDidMount() {
    const { testimony_id } = this.props.match.params;
    const { getTestimony } = this.props;
    getTestimony(testimony_id);
  }

  onDropChange = file => {
    this.setState(
      {
        avatar: file[0]
      },
      () => {
        this.handleImagePreview(this.state.avatar);
      }
    );
  };

  handleImagePreview = avatar => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          avatarPreview: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(avatar);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { updateTestimony, history, testimony } = this.props;
    let testimonyData = new FormData();
    if (this.state.avatar !== testimony.avatar) {
      testimonyData.append('avatar', this.state.avatar);
    }
    testimonyData.append('name', this.state.name);
    testimonyData.append('testimony', this.state.testimony);
    updateTestimony(testimony._id, testimonyData, history);
  };

  componentDidUpdate = async (prevProps, prevState) => {
    if (prevProps.testimony !== this.props.testimony) {
      await this.setState({
        avatar: this.props.testimony.avatar,
        name: this.props.testimony.name,
        testimony: this.props.testimony.testimony,
        avatarPreview: this.props.testimony.avatar
      });
    }
  };

  render() {
    const { name, testimony, avatar, avatarPreview } = this.state;
    return (
      <div className="col-md-6">
        <TestimonyForms
          name={name}
          testimony={testimony}
          avatar={avatar}
          avatarPreview={avatarPreview}
          onDropChange={this.onDropChange}
          onChange={this.onChange}
          onSubmit={this.onSubmit}
          titleName={`Update Testimony`}
          submitButtonName={`Update Testimony`}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  testimony: state.testimonies.testimony
});

const mapDispatchToProps = {
  ...testimonyActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleTestimony);
