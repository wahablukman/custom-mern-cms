import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as testimonyActions from '../../../actions/testimonyActions';
import { connect } from 'react-redux';
import TestimonyItem from '../../../components/admin/testimonies/TestimonyItem';

class IndexTestimony extends Component {
  state = {};

  componentDidMount() {
    const { getAllTestimonies } = this.props;
    getAllTestimonies();
  }

  render() {
    let { testimonies } = this.props;
    testimonies = testimonies.map((testimony, index) => {
      return <TestimonyItem key={index} testimony={testimony} />;
    });
    // console.log(testimonies);
    return (
      <div>
        <h1>Testimonies</h1>
        <br />
        <Link
          to="/admin/create-testimony"
          className="create-post btn btn-primary"
        >
          <i className="fas fa-plus-circle" />
          Create a new testimony
        </Link>
        <div className="flex-parent single-testimony-list table-list header card-custom">
          <div className="testimony">
            <h6>Testimony</h6>
          </div>
          <div className="name">
            <h6>Name</h6>
          </div>
          <div className="avatar">
            <h6>Profile Picture</h6>
          </div>
          <div className="date-created">
            <h6>Created At</h6>
          </div>
        </div>
        {testimonies.length > 0 ? (
          testimonies
        ) : (
          <p className="still-empty">
            You have not created a testimony just yet, &nbsp;
            <Link to="/admin/create-testimony">
              click here to make a new testimony
            </Link>
          </p>
        )}
        <br />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  testimonies: state.testimonies.testimonies
});

const mapDispatchToProps = {
  ...testimonyActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexTestimony);
