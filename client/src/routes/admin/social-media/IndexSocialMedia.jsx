import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as socialMediaActions from '../../../actions/socialMediaActions';
import TextFieldGroup from '../../../components/common/TextFieldGroup';
import isEmpty from '../../../validation/isEmpty';

class IndexSocialMedia extends Component {
  state = {
    facebook: '',
    instagram: '',
    twitter: '',
    youtube: ''
  };

  componentDidMount() {
    const { getSocialMedia } = this.props;
    document.title = 'Social Media';
    getSocialMedia();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !isEmpty(this.props.socialMedia) &&
      prevProps.socialMedia !== this.props.socialMedia
    ) {
      this.setState({
        facebook: this.props.socialMedia.facebook,
        instagram: this.props.socialMedia.instagram,
        twitter: this.props.socialMedia.twitter,
        youtube: this.props.socialMedia.youtube
      });
    }
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { facebook, instagram, twitter, youtube } = this.state;
    const { createOrUpdateSocialMedia } = this.props;
    const socmedData = {
      facebook,
      instagram,
      twitter,
      youtube
    };
    createOrUpdateSocialMedia(socmedData);
  };

  render() {
    // const { errors } = this.props;
    const { facebook, instagram, twitter, youtube } = this.state;
    return (
      <div className="row">
        <div className="col-md-6 mr-auto">
          <h1>SocialMedia</h1>
          <br />
          <form onSubmit={this.onSubmit}>
            <TextFieldGroup
              label="Facebook"
              type="text"
              name="facebook"
              value={facebook}
              hasPrepend="facebook"
              onChange={this.onChange}
              placeholder="Facebook"
            />
            <TextFieldGroup
              label="Instagram"
              type="text"
              name="instagram"
              value={instagram}
              hasPrepend="instagram"
              onChange={this.onChange}
              placeholder="Instagram"
            />
            <TextFieldGroup
              label="Twitter"
              type="text"
              name="twitter"
              value={twitter}
              hasPrepend="twitter"
              onChange={this.onChange}
              placeholder="Twitter"
            />
            <TextFieldGroup
              label="YouTube"
              type="text"
              name="youtube"
              value={youtube}
              hasPrepend="youtube"
              onChange={this.onChange}
              placeholder="YouTube"
            />
            <br />
            <button
              onSubmit={this.onSubmit}
              className="btn btn-primary"
              type="submit"
            >
              <i className="fas fa-paper-plane" />
              Update Your Social Media
            </button>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  socialMedia: state.socialMedia,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  socialMediaActions
)(IndexSocialMedia);
