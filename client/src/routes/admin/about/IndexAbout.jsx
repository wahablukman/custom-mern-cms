import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as aboutActions from '../../../actions/aboutActions';
import WYSIWYG from '../../../components/common/WYSIWYG';

class IndexAbout extends Component {
  state = {
    about: ''
  };

  componentDidMount() {
    document.title = 'About';
    const { getAbout } = this.props;
    getAbout();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.about !== this.props.about) {
      this.setState({
        about: this.props.about
      });
    }
  }

  onWYSIWYGChange = value => {
    this.setState({
      about: value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { createUpdateAbout } = this.props;
    const aboutData = {
      about: this.state.about
    };
    createUpdateAbout(aboutData);
  };

  render() {
    return (
      <div>
        <h1>About</h1>
        <br />
        <form onSubmit={this.onSubmit}>
          <WYSIWYG
            modules={IndexAbout.modules}
            value={this.state.about}
            formats={IndexAbout.formats}
            onChange={this.onWYSIWYGChange}
          />
          <button type="submit" className="btn btn-primary mt-4">
            <i className="fas fa-paper-plane" />
            Update About
          </button>
        </form>
      </div>
    );
  }
}

IndexAbout.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }, { font: [] }],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    ['link', 'image'],
    ['code-block']
  ]
};

IndexAbout.formats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'link',
  'image',
  'video',
  'code-block'
];

const mapStateToProps = state => ({
  about: state.about
});

export default connect(
  mapStateToProps,
  aboutActions
)(IndexAbout);
