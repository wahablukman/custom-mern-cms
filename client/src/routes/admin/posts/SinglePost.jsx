import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../../../actions/postActions';
import isEmpty from '../../../validation/isEmpty';
import renderHTML from 'react-render-html';

class SinglePost extends Component {
  state = {};

  componentWillMount() {
    const { slug } = this.props.match.params;
    const { getPost } = this.props;
    getPost(slug);
  }

  render() {
    // console.log(this.props);
    const { post, isLoading } = this.props;
    let singlePost;
    console.log(isLoading);
    if (isEmpty(post) || isLoading) {
      singlePost = '';
    } else {
      singlePost = (
        <>
          <h2>{post.title}</h2>
          <br />
          {renderHTML(post.content)}
        </>
      );
    }

    return <div>{singlePost}</div>;
  }
}

const mapStateToProps = state => ({
  post: state.posts.post,
  isLoading: state.isLoading
});

export default connect(
  mapStateToProps,
  postActions
)(SinglePost);
