import React, { Component } from 'react';
import { connect } from 'react-redux';
import isEmpty from '../../../validation/isEmpty';
import * as postActions from '../../../actions/postActions';
import * as categoryActions from '../../../actions/categoryActions';
import PostForms from '../../../components/admin/posts/PostForms';

class EditPost extends Component {
  state = {
    title: '',
    content: '',
    categories: [],
    image: '',
    imagePreview: '',
    categoryOptions: []
  };

  componentDidMount() {
    const { slug } = this.props.match.params;
    const { getPost } = this.props;
    const { getAllCategories } = this.props;
    getAllCategories();
    getPost(slug);
  }

  componentWillReceiveProps = async nextProps => {
    // change posts states after getting the props
    if (!isEmpty(nextProps.post.title)) {
      this.setState({
        title: nextProps.post.title,
        content: nextProps.post.content,
        categories: nextProps.post.categories,
        image: nextProps.post.image,
        imagePreview: nextProps.post.image
      });
      document.title = nextProps.post.title;
    }

    // change category options after getting categories from props
    let categoryOptions = [];
    if (!isEmpty(nextProps.categories)) {
      nextProps.categories.map(category => {
        return categoryOptions.push(category.name);
      });
    }
    await this.setState({
      categoryOptions
    });
  };

  handleImagePreview = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          imagePreview: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  selectCategoryOption = value => {
    this.setState({ categories: value });
  };

  onDropChange = file => {
    this.setState(
      {
        image: file[0]
      },
      () => {
        this.handleImagePreview(this.state.image);
      }
    );
  };

  onWYSIWYGChange = value => {
    this.setState({
      content: value
    });
  };

  onSubmit = e => {
    e.preventDefault();

    let { title, content, categories, image } = this.state;
    const { updatePost, history, post } = this.props;
    //categories logic
    isEmpty(categories)
      ? (categories = [])
      : (categories = categories.toString());
    let postData = new FormData();
    if (image !== post.image) postData.append('image', image);
    postData.append('title', title);
    postData.append('content', content);
    postData.append('categories', categories);

    // this.props.match.path === '/create-post'
    //   ? createPost(postData, history)
    //   : updatePost(postData, post._id, history);
    updatePost(postData, post._id, history);
  };

  render() {
    const { errors } = this.props;
    // console.log(this.props.match);
    return (
      <>
        <div className="row">
          <div className="col-md-12 nopadding">
            <PostForms
              onSubmit={this.onSubmit}
              onDropChange={this.onDropChange}
              title={this.state.title}
              onChange={this.onChange}
              errors={errors}
              content={this.state.content}
              modules={EditPost.modules}
              onWYSIWYGChange={this.onWYSIWYGChange}
              categories={this.state.categories}
              categoryOptions={this.state.categoryOptions}
              image={this.state.image}
              selectCategoryOption={this.selectCategoryOption}
              imagePreview={this.state.imagePreview}
              submitButtonName={`Update this post`}
              titleName={`Edit Post`}
            />
          </div>
        </div>
      </>
    );
  }
}

EditPost.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }, { font: [] }],
    [{ align: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ color: [] }, { background: [] }],
    ['link', 'image'],
    ['code-block']
  ]
};

const mapStateToProps = state => ({
  post: state.posts.post,
  categories: state.categories,
  errors: state.errors,
  isLoading: state.isLoading
});

const mapDispatchToProps = {
  ...postActions,
  ...categoryActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditPost);
