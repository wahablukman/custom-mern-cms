import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../../../actions/postActions';
import * as categoryActions from '../../../actions/categoryActions';
import isEmpty from '../../../validation/isEmpty';
import PostForms from '../../../components/admin/posts/PostForms';

class CreatePost extends Component {
  state = {
    title: '',
    content: '',
    categories: [],
    image: '',
    imagePreview: '',
    categoryOptions: []
  };

  componentDidMount() {
    document.title = 'Create a post';
    const { getAllCategories } = this.props;
    getAllCategories();
  }

  componentWillReceiveProps = async nextProps => {
    let categoryOptions = [];
    if (!isEmpty(nextProps.categories)) {
      nextProps.categories.map(category => {
        return categoryOptions.push(category.name);
      });
    }
    await this.setState({
      categoryOptions
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onDropChange = file => {
    this.setState(
      {
        image: file[0]
      },
      () => {
        this.handleImagePreview(this.state.image);
      }
    );
  };

  handleImagePreview = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          imagePreview: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  selectCategoryOption = value => {
    this.setState({ categories: value });
  };

  onWYSIWYGChange = value => {
    this.setState({
      content: value
    });
  };

  onSubmit = e => {
    e.preventDefault();

    let { title, content, categories, image } = this.state;
    const { createPost, history } = this.props;
    //categories logic
    isEmpty(categories)
      ? (categories = [])
      : (categories = categories.toString());
    let postData = new FormData();
    if (image !== '') postData.append('image', image);
    postData.append('title', title);
    postData.append('content', content);
    postData.append('categories', categories);
    createPost(postData, history);
  };

  render() {
    const { errors } = this.props;
    return (
      <>
        <div className="row">
          <div className="col-md-12 nopadding">
            <PostForms
              onSubmit={this.onSubmit}
              onDropChange={this.onDropChange}
              title={this.state.title}
              onChange={this.onChange}
              errors={errors}
              content={this.state.content}
              modules={CreatePost.modules}
              onWYSIWYGChange={this.onWYSIWYGChange}
              categories={this.state.categories}
              categoryOptions={this.state.categoryOptions}
              image={this.state.image}
              selectCategoryOption={this.selectCategoryOption}
              imagePreview={this.state.imagePreview}
              submitButtonName={`Create this post`}
              titleName={`Create Post`}
            />
          </div>
        </div>
      </>
    );
  }
}

CreatePost.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }, { font: [] }],
    [{ align: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ color: [] }, { background: [] }],
    ['link', 'image'],
    ['code-block']
  ]
};

const mapStateToProps = state => ({
  posts: state.posts.posts,
  categories: state.categories,
  errors: state.errors,
  success: state.success
});

const mapDispatchToProps = {
  ...categoryActions,
  ...postActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreatePost);
