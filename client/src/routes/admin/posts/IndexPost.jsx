import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../../../actions/postActions';
import isEmpty from '../../../validation/isEmpty';
import { Link } from 'react-router-dom';
import PostItem from '../../../components/admin/posts/PostItem';
import Pagination from '../../../components/common/Pagination';
import SelectListGroup from '../../../components/common/SelectListGroup';
import queryString from 'query-string';

class IndexPost extends Component {
  state = {
    sortOptions: [
      { label: 'Newest', value: '-createdAt' },
      { label: 'Oldest', value: 'createdAt' },
      { label: 'Title', value: 'normalized' }
    ]
  };

  componentWillMount() {
    const { getAllUserPosts, user } = this.props;
    const parameterQuery = this.props.history.location.search || '';
    getAllUserPosts(parameterQuery, user.id);
    document.title = 'Posts';
  }

  onChangeSort = e => {
    let queryParams = {};
    queryParams.sort = e.target.value;
    this.handleQueryParam(queryParams);
  };

  onClickPagination = pageNumber => {
    let queryParams = {};
    queryParams.page = pageNumber;
    this.handleQueryParam(queryParams);
  };

  // handling query parameters logic
  handleQueryParam = paramData => {
    const { getAllUserPosts, history, user } = this.props;
    let parsedUpdatedQuery;
    if (isEmpty(this.props.history.location.search)) {
      parsedUpdatedQuery = `?${queryString.stringify(paramData)}`;
    } else {
      let parsedCurrentQuery = queryString.parse(
        this.props.history.location.search
      );
      const paramDataKey = Object.keys(paramData)[0];
      const paramDataValue = Object.values(paramData)[0];
      if (paramDataKey in parsedCurrentQuery) {
        delete parsedCurrentQuery[paramDataKey];
        parsedCurrentQuery[paramDataKey] = paramDataValue;
        parsedUpdatedQuery = `?${queryString.stringify(parsedCurrentQuery)}`;
      } else {
        parsedUpdatedQuery = `${
          this.props.history.location.search
        }&${queryString.stringify(paramData)}`;
      }
    }
    history.push(parsedUpdatedQuery);
    getAllUserPosts(parsedUpdatedQuery, user.id);
  };

  onDelete = postId => {
    const { deletePost } = this.props;
    deletePost(postId);
  };

  render() {
    const { posts, isLoading } = this.props;
    const { sortOptions } = this.state;
    const { totalPages, currentPage } = this.props.pagination;

    // console.log(this.props);

    const paginationButtons = Array.apply(null, {
      length: totalPages
    }).map((value, index) => {
      index = index + 1;
      return (
        <Pagination
          currentPage={currentPage}
          key={index}
          index={index}
          onClick={() => this.onClickPagination(index)}
        />
      );
    });

    let allPosts;
    if (isEmpty(posts) || isLoading) {
      allPosts = '';
    } else {
      allPosts = posts.map((post, index) => (
        <PostItem
          onDelete={() => this.onDelete(post._id)}
          post={post}
          key={index}
        />
      ));
    }

    //whole posts page
    return (
      <div>
        <h1>Posts</h1>
        <Link to="/admin/create-post" className="create-post btn btn-primary">
          <i className="fas fa-plus-circle" />
          Create a new post
        </Link>
        <div className="col-md-2 mr-auto nopadding">
          <SelectListGroup
            name="sort"
            options={sortOptions}
            onChange={this.onChangeSort}
            placeholder="Sort by"
            label="Sort by:"
          />
        </div>
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            {totalPages > 1 ? paginationButtons : null}
          </ul>
        </nav>
        <div className="flex-parent single-post-list table-list header card-custom">
          <div className="title">
            <h6>Title</h6>
          </div>
          <div className="author">
            <h6>Author</h6>
          </div>
          <div className="categories">
            <h6>Category</h6>
          </div>
          <div className="date-created">
            <h6>Created At</h6>
          </div>
        </div>
        {allPosts}
        {isEmpty(posts) ? (
          <p className="still-empty">
            You have not posted anything, &nbsp;
            <Link to="/admin/create-post"> click here to make a new post</Link>
          </p>
        ) : null}
        <br />
        <nav aria-label="Page navigation example">
          <ul className="pagination">
            {totalPages > 1 ? paginationButtons : null}
          </ul>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
  posts: state.posts.posts,
  pagination: {
    totalPages: state.posts.pagination.totalPages,
    currentPage: state.posts.pagination.page,
    hasPrevPage: state.posts.pagination.hasPrevPage,
    hasNextPage: state.posts.pagination.hasNextPage
  },
  isLoading: state.isLoading
});

export default connect(
  mapStateToProps,
  postActions
)(IndexPost);
