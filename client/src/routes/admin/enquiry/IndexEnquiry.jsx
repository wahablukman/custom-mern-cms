import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as enquiryActions from '../../../actions/enquiryActions';
import EnquiryItem from '../../../components/admin/enquiries/EnquiryItem';
import EnquiryModal from '../../../components/admin/enquiries/EnquiryModal';
import isEmpty from '../../../validation/isEmpty';

class IndexEnquiry extends Component {
  state = {
    enquiry: {}
  };

  componentDidMount() {
    const { getAllEnquiries } = this.props;
    getAllEnquiries();
  }

  onClickShowEnquiry = async enquiryId => {
    const { getEnquiry } = this.props;
    await getEnquiry(enquiryId);
  };

  onClickCloseModal = () => {
    if (!isEmpty(this.state.enquiry)) {
      this.setState({
        enquiry: {}
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.enquiry !== this.props.enquiry) {
      this.setState({
        enquiry: this.props.enquiry
      });
    }
  }

  onClickDelete = enquiryId => {
    const { deleteEnquiry } = this.props;
    deleteEnquiry(enquiryId);
  };

  render() {
    let { enquiries } = this.props;
    enquiries = enquiries.map((enquiry, index) => {
      return (
        <EnquiryItem
          onClickDelete={() => this.onClickDelete(enquiry._id)}
          onClickShowEnquiry={() => this.onClickShowEnquiry(enquiry._id)}
          enquiry={enquiry}
          key={index}
        />
      );
    });
    return (
      <>
        <div className="enquiry-section">
          <h1>Enquiry</h1>
          <br />
          {!isEmpty(this.state.enquiry) ? (
            <div className="enquiry-modal animated fadeIn">
              <div className="enquiry-modal-content">
                <EnquiryModal
                  enquiry={this.state.enquiry}
                  onClickCloseModal={this.onClickCloseModal}
                />
              </div>
            </div>
          ) : null}
          <div className="flex-parent single-enquiry-list table-list header card-custom">
            <div className="message">
              <h6>Message</h6>
            </div>
            <div className="name">
              <h6>Name</h6>
            </div>
            <div className="email">
              <h6>E-mail</h6>
            </div>
            <div className="date-created">
              <h6>Created At</h6>
            </div>
          </div>
          {enquiries.length > 0 ? (
            enquiries
          ) : (
            <p className="still-empty">You don't have any enquiry just yet</p>
          )}
          <br />
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  enquiries: state.enquiries.enquiries,
  pagination: {
    totalPages: state.enquiries.pagination.totalPages,
    currentPage: state.enquiries.pagination.page,
    hasPrevPage: state.enquiries.pagination.hasPrevPage,
    hasNextPage: state.enquiries.pagination.hasNextPage
  },
  enquiry: state.enquiries.enquiry
});

const mapDispatchToProps = {
  ...enquiryActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexEnquiry);
