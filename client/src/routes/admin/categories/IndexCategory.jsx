import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as categoryActions from '../../../actions/categoryActions';
import TextFieldGroup from '../../../components/common/TextFieldGroup';

class IndexCategories extends Component {
  state = {
    name: ''
  };

  componentDidMount() {
    const { getAllCategories } = this.props;
    document.title = 'Categories';
    getAllCategories();
  }

  onSubmit = async e => {
    const { createCategory } = this.props;
    e.preventDefault();
    const newCategory = {
      name: this.state.name
    };
    await createCategory(newCategory);
    this.setState({
      name: ''
    });
  };

  onDelete = categoryId => {
    const { deleteCategory } = this.props;
    deleteCategory(categoryId);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const { categories, errors } = this.props;
    const category = categories.map((category, index) => {
      return (
        <div
          className="col-md-2 mr-1 mb-1 category"
          key={index}
          onClick={() => this.onDelete(category._id)}
        >
          <p>{category.name}</p>
        </div>
      );
    });
    return (
      <div className="categories-page">
        <h1>Categories</h1>
        <div className="alert alert-info" role="alert">
          * Click the category to delete them
        </div>
        <br />
        <h2>Available categories are:</h2>
        <br />
        <div className="row">{category}</div>
        <br />
        <br />
        <h2>Create new category</h2>
        <form onSubmit={this.onSubmit} className="col-md-3 nopadding">
          <br />
          <TextFieldGroup
            type="text"
            name="name"
            placeholder="Add your new category here"
            onChange={this.onChange}
            error={errors.name}
            value={this.state.name}
          />
          <button className="btn btn-primary" type="submit">
            Add category
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categories,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  categoryActions
)(IndexCategories);
