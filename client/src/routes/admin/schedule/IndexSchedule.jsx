import React, { Component } from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import '../../../../node_modules/react-big-calendar/lib/css/react-big-calendar.css';

const localizer = BigCalendar.momentLocalizer(moment);

class IndexSchedule extends Component {
  state = {};

  componentDidMount() {
    document.title = 'Schedule';
    moment.locale('id');
  }

  onSelectEvent = e => {
    console.log(e);
  };

  render() {
    const events = [
      {
        title: 'Manteb',
        start: moment('2018-12-09T03:01:00.318Z').toDate(),
        end: moment('2018-12-09T03:01:00.318Z').toDate(),
        allDay: false,
        selectable: true
      },
      {
        title: 'Mantebb',
        start: moment('2018-12-09T07:00:00.318Z').toDate(),
        end: moment('2018-12-09T09:00:00.318Z').toDate(),
        allDay: false,
        selectable: true
      },
      {
        title: 'Mantebbb',
        start: moment('2018-12-09T10:00:00.318Z').toDate(),
        end: moment('2018-12-09T10:30:00.318Z').toDate(),
        allDay: false,
        selectable: true
      },
      {
        title: 'Manteb 2',
        start: new Date(),
        end: new Date(),
        allDay: true,
        selectable: true
      }
    ];
    return (
      <div>
        <h1>Schedule</h1>
        <BigCalendar
          localizer={localizer}
          events={events}
          selectable={true}
          onSelectSlot={this.onSelectEvent}
        />
      </div>
    );
  }
}

export default IndexSchedule;
