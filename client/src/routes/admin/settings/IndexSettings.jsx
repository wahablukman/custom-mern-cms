import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../../actions/authActions';
import * as mainProfileActions from '../../../actions/mainProfileActions';
import classnames from 'classnames';
import PersonalProfile from '../../../components/admin/settings/PersonalProfile';
import MainProfile from '../../../components/admin/settings/MainProfile';

class IndexSettings extends Component {
  state = {
    name: '',
    avatar: '',
    role: '',
    newAvatar: {},
    avatarPreview: '',
    personalProfileSettings: true,
    mainProfileSettings: false
  };

  componentDidMount() {
    const { auth, getMainProfile } = this.props;
    this.setState({
      name: auth.user.name,
      avatar: auth.user.avatar,
      role: auth.user.role
    });
    getMainProfile();
  }

  onClickPersonalProfileSettings = () => {
    this.setState({
      personalProfileSettings: true,
      mainProfileSettings: false
    });
  };

  onClickMainProfileSettings = () => {
    this.setState({
      personalProfileSettings: false,
      mainProfileSettings: true
    });
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onDropChange = file => {
    this.setState(
      {
        newAvatar: file[0]
      },
      () => {
        this.handleImagePreview(this.state.newAvatar);
      }
    );
  };

  handleImagePreview = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          avatar: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  onSubmitPersonalProfile = e => {
    const { updateUserDetails, auth } = this.props;
    e.preventDefault();
    let userData = new FormData();
    userData.append('name', this.state.name);
    userData.append('role', this.state.role);

    if (auth.user.avatar !== this.state.avatar) {
      userData.append('avatar', this.state.newAvatar);
    }

    return updateUserDetails(userData);
  };

  render() {
    const { name, avatar, role } = this.state;
    const { user } = this.props.auth;
    return (
      <div className="settings-section col-md-6">
        <h1>Settings</h1>
        <br />
        {user.role === 'Admin' ? (
          <nav className="sub-nav mb-5">
            <li>
              <button
                onClick={this.onClickPersonalProfileSettings}
                className={classnames('btn', {
                  active: this.state.personalProfileSettings
                })}
              >
                Your Profile
              </button>
            </li>
            <li>
              <button
                onClick={this.onClickMainProfileSettings}
                className={classnames('btn', {
                  active: this.state.mainProfileSettings
                })}
              >
                Main Profile
              </button>
            </li>
          </nav>
        ) : null}
        {this.state.personalProfileSettings ? (
          <PersonalProfile
            name={name}
            avatar={avatar}
            role={role}
            onDropChange={this.onDropChange}
            onSubmit={this.onSubmitPersonalProfile}
            onChange={this.onChange}
          />
        ) : null}

        {this.state.mainProfileSettings ? <MainProfile /> : null}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  mainProfile: state.mainProfile
});

const mapDispatchToProps = {
  ...authActions,
  ...mainProfileActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IndexSettings);
