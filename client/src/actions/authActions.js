import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import {
  SET_CURRENT_USER,
  GET_ERRORS,
  GET_SUCCESS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  IS_LOADING
} from './types';

// Login - Get User
export const loginUser = (userData, history) => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .post('/api/auth/login', userData)
    .then(res => {
      //save to local storage
      const { token } = res.data;
      //set token to local storage
      localStorage.setItem('jwtToken', token);
      //set token to auth header
      setAuthToken(token);
      //decode token to get user data
      const decoded = jwt_decode(token);
      //set current user
      dispatch(setCurrentUser(decoded));
      //give success message
      dispatch(getSuccessMessage('Login success!'));
      //clear any initial errors
      dispatch(clearErrors());
      //redirect after success login
      history.push('/admin/dashboard');
      dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

export const updateUserDetails = userData => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    const updateUser = await axios.put('/api/auth/edit-auth-details', userData);
    const { newToken } = updateUser.data;
    //set token to local storage
    localStorage.setItem('jwtToken', newToken);
    //set token to auth header
    setAuthToken(newToken);
    //decode token to get user data
    const decoded = jwt_decode(newToken);
    //set current user
    dispatch(setCurrentUser(decoded));
    dispatch(isNowLoading(false));
    //give success message
    dispatch(getSuccessMessage('Succesfully updated your profile!'));
    //clear any initial errors
    return dispatch(clearErrors());
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch({
      type: GET_ERRORS,
      payload: err
    });
  }
};

//set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// log user out
export const logoutUser = () => dispatch => {
  // remove token from localStorage
  localStorage.removeItem('jwtToken');
  // remove auth header for future requests
  setAuthToken(false);
  // set current user to {} which will also set isAuthenticated to false
  dispatch(setCurrentUser({}));
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

//clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

//clear success messages
export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
