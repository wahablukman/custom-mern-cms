import axios from 'axios';
import {
  GET_ERRORS,
  GET_SUCCESS,
  IS_LOADING,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  GET_ALL_TESTIMONIES,
  GET_TESTIMONY,
  CREATE_TESTIMONY,
  UPDATE_TESTIMONY
} from './types';

// get all testimonies
export const getAllTestimonies = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const getAllTestimonies = await axios.get('/api/testimony');
    dispatch(clearErrors());
    dispatch({
      type: GET_ALL_TESTIMONIES,
      payload: getAllTestimonies.data
    });
    return dispatch(isNowLoading(false));
  } catch (error) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(error.response.data.message));
  }
};

// get a testimony
export const getTestimony = testimonyId => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const getTestimony = await axios.get(`/api/testimony/${testimonyId}`);
    dispatch(clearErrors());
    dispatch({
      type: GET_TESTIMONY,
      payload: getTestimony.data
    });
    return dispatch(isNowLoading(false));
  } catch (error) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(error.response.data.message));
  }
};

// create testimony
export const createTestimony = (testimonyData, history) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const createTestimony = await axios.post('/api/testimony', testimonyData);
    dispatch({
      type: CREATE_TESTIMONY,
      payload: createTestimony.data
    });
    dispatch(clearErrors());
    dispatch(getSuccessMessage('Testimony succesfully created'));
    dispatch(isNowLoading(false));
    return history.push('/admin/testimony');
  } catch (error) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(error.response.data.message));
  }
};

// update testimony
export const updateTestimony = (
  testimonyId,
  testimonyData,
  history
) => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const updateTestimony = await axios.put(
      `/api/testimony/${testimonyId}`,
      testimonyData
    );
    dispatch({
      type: UPDATE_TESTIMONY,
      payload: updateTestimony.data
    });
    dispatch(clearErrors());
    dispatch(getSuccessMessage('Testimony succesfully updated'));
    dispatch(isNowLoading(false));
    return history.push('/admin/testimony');
  } catch (error) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(error.response.data.message));
  }
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
