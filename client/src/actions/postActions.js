import axios from 'axios';
import {
  GET_ALL_POSTS,
  GET_POST,
  GET_ERRORS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  ADD_POST,
  GET_SUCCESS,
  IS_LOADING,
  EDIT_POST,
  DELETE_POST
} from './types';

// get all posts
export const getAllPosts = parameterQuery => dispatch => {
  dispatch(isNowLoading(true));
  parameterQuery = parameterQuery || '';
  axios
    .get(`/api/posts/all${parameterQuery}`)
    .then(res => {
      dispatch({
        type: GET_ALL_POSTS,
        payload: res.data
      });
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

// get all posts
export const getAllUserPosts = (parameterQuery, userId) => dispatch => {
  dispatch(isNowLoading(true));
  parameterQuery = parameterQuery || '';
  axios
    .get(`/api/posts/all-user/${userId}${parameterQuery}`)
    .then(res => {
      dispatch({
        type: GET_ALL_POSTS,
        payload: res.data
      });
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

// get post by slug
export const getPost = slug => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .get(`/api/posts/slug/${slug}`)
    .then(res => {
      dispatch({
        type: GET_POST,
        payload: res.data
      });
      dispatch(clearErrors());
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

// create a new post
export const createPost = (postData, history) => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .post('/api/posts', postData)
    .then(res => {
      dispatch({
        type: ADD_POST,
        payload: res.data
      });
      dispatch(clearErrors());
      dispatch(getSuccessMessage('Your post is succesfully created'));
      history.push('/admin/posts');
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

// update post
export const updatePost = (postData, postId, history) => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .post(`/api/posts/${postId}`, postData)
    .then(res => {
      dispatch({
        type: EDIT_POST,
        payload: res.data
      });
      dispatch(clearErrors());
      dispatch(getSuccessMessage('Your post has been updated'));
      history.push('/admin/posts');
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

// Delete post
export const deletePost = postId => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .delete(`/api/posts/${postId}`)
    .then(() => {
      dispatch({
        type: DELETE_POST,
        payload: postId
      });
      dispatch(clearErrors());
      dispatch(getSuccessMessage('Your post has been deleted'));
      dispatch(isNowLoading(false));
      return dispatch(getAllUserPosts());
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
