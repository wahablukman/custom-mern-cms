import axios from 'axios';
import {
  GET_SOCIAL_MEDIA,
  IS_LOADING,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  GET_ERRORS,
  CREATE_OR_UPDATE_SOCIAL_MEDIA,
  GET_SUCCESS
} from './types';

// get social media
export const getSocialMedia = () => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .get('/api/social-media')
    .then(res => {
      dispatch(clearErrors());
      dispatch({
        type: GET_SOCIAL_MEDIA,
        payload: res.data
      });
      dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err
      });
      dispatch(isNowLoading(false));
    });
};

export const createOrUpdateSocialMedia = socmedData => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .post('/api/social-media', socmedData)
    .then(res => {
      dispatch(clearErrors());
      dispatch({
        type: CREATE_OR_UPDATE_SOCIAL_MEDIA,
        payload: res.data
      });
      dispatch(isNowLoading(false));
      dispatch(getSuccessMessage('Your social media has been updated'));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      dispatch({
        type: GET_ERRORS,
        payload: err
      });
    });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
