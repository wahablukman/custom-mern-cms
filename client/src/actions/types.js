// error
export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';

// success
export const GET_SUCCESS = 'GET_SUCCESS';
export const CLEAR_SUCCESS = 'CLEAR_SUCCESS';

// about
export const GET_ABOUT = 'EDIT_ABOUT';
export const CREATE_OR_UPDATE_ABOUT = 'CREATE_OR_UPDATE_ABOUT';

// posts
export const GET_ALL_POSTS = 'GET_ALL_POSTS';
export const GET_POST = 'GET_POST';
export const ADD_POST = 'ADD_POST';
export const EDIT_POST = 'EDIT_POST';
export const DELETE_POST = 'DELETE POST';

// social media
export const GET_SOCIAL_MEDIA = 'GET_SOCIAL_MEDIA';
export const CREATE_OR_UPDATE_SOCIAL_MEDIA = 'CREATE_OR_UPDATE_SOCIAL_MEDIA';

// categories
export const GET_ALL_CATEGORIES = 'GET_ALL_CATEGORIES';
export const CREATE_CATEGORY = 'CREATE_CATEGORY';
export const DELETE_CATEGORY = 'DELETE_CATEGORY';

//main profile
export const GET_MAIN_PROFILE = 'GET_MAIN_PROFILE';
export const CREATE_OR_UPDATE_MAIN_PROFILE = 'CREATE_OR_UPDATE_MAIN_PROFILE';

// enquiries
export const GET_ALL_ENQUIRIES = 'GET_ALL_ENQUIRIES';
export const GET_ENQUIRY = 'GET_ENQUIRY';
export const DELETE_ENQUIRY = 'DELETE_ENQUIRY';

// testimonies
export const CREATE_TESTIMONY = 'CREATE_TESTIMONY';
export const UPDATE_TESTIMONY = 'UPDATE_TESTIMONY';
export const GET_ALL_TESTIMONIES = 'GET_ALL_TESTIMONIES';
export const GET_TESTIMONY = 'GET_TESTIMONY';
export const DELETE_TESTIMONY = 'DELETE_TESTIMONY';

// loading
export const IS_LOADING = 'IS_LOADING';

export const SET_CURRENT_USER = 'SET_CURRENT_USER';
