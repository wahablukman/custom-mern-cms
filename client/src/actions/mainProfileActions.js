import axios from 'axios';
import {
  GET_SUCCESS,
  CLEAR_SUCCESS,
  CLEAR_ERRORS,
  IS_LOADING,
  GET_ERRORS,
  GET_MAIN_PROFILE,
  CREATE_OR_UPDATE_MAIN_PROFILE
} from './types';

// fetch main profile
export const getMainProfile = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const findMainProfile = await axios.get('/api/main-profile');
    dispatch({
      type: GET_MAIN_PROFILE,
      payload: findMainProfile.data
    });
    dispatch(clearErrors());
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err.response.data.message));
  }
};

// create if profile doesnt exist, update if it does
export const createOrUpdateMainProfile = mainProfileData => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const createOrUpdateMainProfile = await axios.post(
      '/api/main-profile',
      mainProfileData
    );
    dispatch({
      type: CREATE_OR_UPDATE_MAIN_PROFILE,
      payload: createOrUpdateMainProfile.data
    });
    dispatch(clearErrors());
    dispatch(getSuccessMessage('Main profile updated!'));
    return dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(err.response.data.message));
  }
};

export const getErrorMessage = errorMessage => dispatch => {
  dispatch({
    type: GET_ERRORS,
    payload: errorMessage
  });
  setTimeout(() => {
    dispatch(clearErrors());
  }, 3000);
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
