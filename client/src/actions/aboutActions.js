import axios from 'axios';
import {
  GET_ABOUT,
  GET_ERRORS,
  CLEAR_ERRORS,
  CREATE_OR_UPDATE_ABOUT,
  GET_SUCCESS,
  CLEAR_SUCCESS,
  IS_LOADING
} from './types';

export const getAbout = () => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .get('/api/about')
    .then(res => {
      dispatch({
        type: GET_ABOUT,
        payload: res.data.about
      });
      dispatch(clearErrors());
      return dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      // return console.log(err);
      return dispatch({
        type: GET_ERRORS,
        payload: err.response.message ? err.response.message : err
      });
    });
};

export const createUpdateAbout = aboutData => dispatch => {
  dispatch(isNowLoading(true));
  axios
    .post('/api/about', aboutData)
    .then(res => {
      dispatch({
        type: CREATE_OR_UPDATE_ABOUT,
        payload: res.data.about
      });
      console.log(res.data);
      dispatch(clearErrors());
      dispatch(getSuccessMessage('About section has been updated succesfully'));
      dispatch(isNowLoading(false));
    })
    .catch(err => {
      dispatch(isNowLoading(false));
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data ? err.response.data : err
      });
    });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};
