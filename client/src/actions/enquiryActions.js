import axios from 'axios';
import {
  GET_ERRORS,
  GET_SUCCESS,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  IS_LOADING,
  GET_ALL_ENQUIRIES,
  DELETE_ENQUIRY,
  GET_ENQUIRY
} from './types';
import { getErrorMessage } from './mainProfileActions';

// Get all enquiries
export const getAllEnquiries = () => async dispatch => {
  dispatch(isNowLoading(true));

  try {
    const getAllEnquiries = await axios.get('/api/enquiry');
    // return console.log(getAllEnquiries.data);
    dispatch(clearErrors());
    dispatch({
      type: GET_ALL_ENQUIRIES,
      payload: getAllEnquiries.data
    });
    return dispatch(isNowLoading(false));
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data.message
    });
    return dispatch(isNowLoading(false));
  }
};

// Get selected enquiry
export const getEnquiry = enquiryId => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const getEnquiry = await axios.get(`/api/enquiry/${enquiryId}`);
    dispatch(clearErrors());
    dispatch({
      type: GET_ENQUIRY,
      payload: getEnquiry.data
    });
    return dispatch(isNowLoading(false));
  } catch (error) {
    dispatch(isNowLoading(false));
    return dispatch(getErrorMessage(error.response.data.message));
  }
};

// delete enquiry
export const deleteEnquiry = enquiryId => async dispatch => {
  dispatch({
    type: DELETE_ENQUIRY,
    payload: enquiryId
  });
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
