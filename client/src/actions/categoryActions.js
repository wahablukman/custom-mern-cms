import axios from 'axios';
import {
  GET_ALL_CATEGORIES,
  GET_ERRORS,
  IS_LOADING,
  CLEAR_ERRORS,
  CLEAR_SUCCESS,
  CREATE_CATEGORY,
  GET_SUCCESS,
  DELETE_CATEGORY
} from './types';

export const getAllCategories = () => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.get('/api/category');
    dispatch({
      type: GET_ALL_CATEGORIES,
      payload: response.data
    });
    dispatch(isNowLoading(false));
    dispatch(clearErrors());
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch({
      type: GET_ERRORS,
      payload: err.response.data ? err.response.data : err
    });
  }
};

export const createCategory = newCategory => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    const response = await axios.post(`/api/category`, newCategory);
    dispatch({
      type: CREATE_CATEGORY,
      payload: response.data
    });
    dispatch(getSuccessMessage('New category is succesfully added'));
    dispatch(clearErrors());
    dispatch(isNowLoading(false));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch({
      type: GET_ERRORS,
      payload: err.response.data ? err.response.data : err
    });
  }
};

export const deleteCategory = categoryId => async dispatch => {
  dispatch(isNowLoading(true));
  try {
    await axios.post(`/api/category/${categoryId}`);
    dispatch({
      type: DELETE_CATEGORY,
      payload: categoryId
    });
    dispatch(clearErrors());
    dispatch(isNowLoading(false));
    dispatch(getSuccessMessage('Category deleted'));
  } catch (err) {
    dispatch(isNowLoading(false));
    return dispatch({
      type: GET_ERRORS,
      payload: err.response.data ? err.response.data : err
    });
  }
};

export const getSuccessMessage = successMessage => dispatch => {
  dispatch({
    type: GET_SUCCESS,
    payload: successMessage
  });
  setTimeout(() => {
    dispatch(clearSuccess());
  }, 3000);
};

export const isNowLoading = loadingState => dispatch => {
  dispatch({
    type: IS_LOADING,
    payload: loadingState
  });
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};

export const clearSuccess = () => {
  return {
    type: CLEAR_SUCCESS
  };
};
