const cloudinary = require('cloudinary');
const keys = require('./keys');

module.exports = cloudinary.config({
  cloud_name: keys.cloudinary_cloud_name,
  api_key: keys.cloudinary_api_key,
  api_secret: keys.cloudinary_api_secret
});
