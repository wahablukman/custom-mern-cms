const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LogoSchema = new Schema(
  {
    image: {
      type: String
    },
    imageID: {
      type: String
    },
    text: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = Logo = mongoose.model('logo', LogoSchema);
