const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

const EnquirySchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    email: {
      type: String
    },
    phone: {
      type: String
    },
    message: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

EnquirySchema.plugin(mongoosePaginate);

module.exports = Enquiry = mongoose.model('enquiry', EnquirySchema);
