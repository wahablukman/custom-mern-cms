const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AboutSchema = new Schema(
  {
    about: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = About = mongoose.model('about', AboutSchema);
