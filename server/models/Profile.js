const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProfileSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    logo: {
      type: String
    },
    logoId: {
      type: String
    },
    email: {
      type: String,
      required: true
    },
    socialMedia: {
      instagram: {
        type: String
      },
      twitter: {
        type: String
      },
      facebook: {
        type: String
      },
      linkedin: {
        type: String
      },
      youtube: {
        type: String
      },
      soundcloud: {
        type: String
      }
    }
  },
  { timestamps: true }
);

module.exports = Profile = mongoose.model('profile', ProfileSchema);
