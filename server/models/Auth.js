const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AuthSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      required: true,
      default: '/default-assets/default-avatar.jpg'
    },
    avatarId: {
      type: String
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    role: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

module.exports = Auth = mongoose.model('auth', AuthSchema);
