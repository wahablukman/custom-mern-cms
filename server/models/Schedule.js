const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ScheduleSchema = new Schema(
  {
    from: {
      type: Date,
      required: true
    },
    to: {
      type: Date
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = Schedule = mongoose.model('schedule', ScheduleSchema);
