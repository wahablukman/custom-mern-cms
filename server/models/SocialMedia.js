const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SocialMediaSchema = new Schema(
  {
    instagram: {
      type: String
    },
    youtube: {
      type: String
    },
    twitter: {
      type: String
    },
    facebook: {
      type: String
    }
  },
  { timestamps: true }
);

module.exports = SocialMedia = mongoose.model('socialMedia', SocialMediaSchema);
