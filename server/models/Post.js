const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

const PostSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'auth'
    },
    image: {
      type: String,
      default: 'defaultFeaturedImage.jpg'
    },
    imageID: {
      type: String
    },
    author: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    normalized: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      required: true
    },
    content: {
      type: String,
      required: true
    },
    categories: {
      type: [String]
    }
  },
  { timestamps: true }
);

PostSchema.plugin(mongoosePaginate);

module.exports = Post = mongoose.model('post', PostSchema);
