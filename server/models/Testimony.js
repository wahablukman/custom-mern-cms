const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate-v2');

const TestimonySchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    avatar: {
      type: String,
      required: true
    },
    avatarId: {
      type: String,
      required: true
    },
    testimony: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
);

TestimonySchema.plugin(mongoosePaginate);

module.exports = Testimony = mongoose.model('testimony', TestimonySchema);
