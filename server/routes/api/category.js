const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');

//load required model(s)
const Category = require('../../models/Category');

router.get('/test', (req, res) => {
  res.json({ msg: 'success!' });
});

// @route   GET api/category
// @desc    Get all categories
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};
  try {
    const categories = await Category.find();
    return res.json(categories);
  } catch (err) {
    errors.message = 'server error, cannot find categories right now';
    return res.status(500).json(errors);
  }
});

// @route   GET api/category/all-in-string
// @desc    Get all categories in strong - NOT object
// @access  Public
router.get('/all-in-string', async (req, res) => {
  let errors = {};
  try {
    const categories = await Category.find();
    if (!isEmpty(categories)) {
      const singleCategory = categories.map(category => {
        return category.name;
      });
      return res.json(singleCategory);
    } else {
      errors.notfound = 'No categories created yet';
      return res.json(errors);
    }
  } catch (err) {
    return res.status(400).json(err);
  }
});

// @route   POST api/category
// @desc    Create category
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    const newCategory = new Category({
      name: req.body.name
    });

    if (!isEmpty(req.body.name)) {
      try {
        const newCategorySubmitted = await newCategory.save();
        return res.json(newCategorySubmitted);
      } catch (err) {
        errors.message = 'Server error, cannot add category right now';
        return res.status(500).json(errors);
      }
    } else {
      errors.name = 'Category cannot be empty';
      return res.status(400).json(errors);
    }
  }
);

// @route   DELETE api/category/:id
// @desc    Delete category
// @access  Private
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    try {
      await Category.findOneAndRemove({ _id: req.params.id });
      return res.json({ message: 'succesfully deleted' });
    } catch (err) {
      errors.message = 'server error, cannot delete category';
      return res.status(500).json(errors);
    }
  }
);

module.exports = router;
