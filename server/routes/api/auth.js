const express = require('express');
const router = express.Router();
const keys = require('../../config/keys');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');
const cloudinary = require('cloudinary');

require('../../config/cloudinary');

// load models required
const Auth = require('../../models/Auth');

// load validations required
const validateLoginInput = require('../../validation/login.validation');
const validateRegisterInput = require('../../validation/register.validation');

// @route   GET api/auth/test
// @desc    test users route
// @access  Public
router.get('/test', (req, res) => {
  res.json({ msg: 'users works' });
});

// @route   POST api/auth/register
// @desc    registering user to database
// @access  Public
router.post('/register', async (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  if (!isValid) {
    return res.status(400).json(errors);
  }

  const newUser = new Auth({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    role: req.body.role
  });

  //check if email already exists
  const findUser = await Auth.findOne({ email: req.body.email });
  if (findUser) {
    errors.email = 'Email already exists';
    return res.status(400).json(errors);
  } else {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) throw err;
        newUser.password = hash;
        newUser
          .save()
          .then(user => {
            return res.json(user);
          })
          .catch(err => {
            console.log(err);
          });
      });
    });
  }
});

// @route   POST api/auth/login
// @desc    Logging in user and handling auth
// @access  Public
router.post('/login', async (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  //find user by email
  const findUser = await Auth.findOne({ email });

  if (!findUser) {
    errors.email = 'User not found';
    return res.status(404).json(errors);
  } else {
    // check password
    bcrypt.compare(password, findUser.password).then(isMatch => {
      if (isMatch) {
        // User matched
        // create jwt payload
        const payload = {
          id: findUser.id,
          name: findUser.name,
          avatar: findUser.avatar,
          role: findUser.role
        };

        // Sign token - 604800 = a week
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: 604800 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            });
          }
        );
      } else {
        errors.password = 'password incorrect';
        return res.status(400).json(errors);
      }
    });
  }
});

// @route   PUT api/auth/:id
// @desc    Update auth details
// @access  Private
router.put(
  '/edit-auth-details',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const authData = {
      name: req.body.name,
      role: req.body.role
    };

    if (!isEmpty(req.files.avatar)) {
      //delete current avatar on cloudinary if avatar is not default
      if (
        req.user.avatar !== '/default-assets/default-avatar.jpg' ||
        req.user.avatar !== 'defaultURL.jpg'
      ) {
        await cloudinary.v2.uploader.destroy(req.user.avatarId);
      }
      const cloudinaryOptions = {
        overwrite: true,
        folder: 'profile_pict'
      };
      const uploadAvatar = await cloudinary.v2.uploader.upload(
        req.files.avatar.path,
        cloudinaryOptions
      );
      authData.avatar = uploadAvatar.url;
      authData.avatarId = uploadAvatar.public_id;
    }

    try {
      // const findUser = await Auth.findOne({ _id: req.user._id });
      const findUserAndUpdate = await Auth.findOneAndUpdate(
        { _id: req.user.id },
        { $set: authData },
        { new: true }
      );

      let newToken;
      if (findUserAndUpdate) {
        // sign new token
        const payload = {
          id: findUserAndUpdate.id,
          name: findUserAndUpdate.name,
          avatar: findUserAndUpdate.avatar,
          avatarId: findUserAndUpdate.avatarId,
          role: findUserAndUpdate.role
        };
        // Sign token - 604800 = a week
        newToken = await jwt.sign(payload, keys.secretOrKey, {
          expiresIn: 604800
        });
      }

      return res.json({
        updatedUser: findUserAndUpdate,
        newToken: `Bearer ${newToken}`
      });

      // return res.json({ user: findUserAndUpdate, newToken: signNewJwt });
    } catch (err) {
      errors.message = 'Server error cannot update your user details';
      return res.status(500).json(errors);
    }
  }
);

// @route   GET api/auth/current-user
// @desc    retrieve current user info
// @access  Private
router.get(
  '/current-user',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    let errors = {};
    const auth = req.user;
    return res.json(auth);
    // res.json({
    //   id: auth.id,
    //   name: req.user.name,
    //   email: req.user.email
    // });
  }
);

module.exports = router;
