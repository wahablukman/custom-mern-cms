const express = require('express');
const router = express.Router();
const passport = require('passport');
const keys = require('../../config/keys');
const cloudinary = require('cloudinary');
const isEmpty = require('../../validation/isEmpty.validation');

require('../../config/cloudinary');

// load required models
const Profile = require('../../models/Profile');

// @route   GET api/main-profile
// @desc    get the main profile
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};

  try {
    const findProfile = await Profile.findOne();
    if (findProfile) {
      return res.json(findProfile);
    } else {
      errors.message = 'Profile has not been created yet';
      return res.status(404).json(errors);
    }
  } catch (err) {
    errors.message = 'Server Error, cannot retrieve profile';
    return res.status(500).json(errors);
  }
});

// @route   POST api/main-profile
// @desc    Create or update main business's profile
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const profileData = {
      name: req.body.name,
      email: req.body.email,
      socialMedia: {
        instagram: req.body.instagram,
        twitter: req.body.twitter,
        facebook: req.body.facebook,
        linkedin: req.body.linkedin,
        soundcloud: req.body.soundcloud,
        youtube: req.body.youtube
      }
    };

    const findProfile = await Profile.findOne();

    // only admin allowed to update main profile
    if (req.user.role === 'Admin') {
      if (req.files.logo) {
        try {
          const cloudinaryOptions = {
            overwrite: true,
            folder: 'main-profile'
          };

          if (!isEmpty(findProfile.logoId)) {
            await cloudinary.v2.uploader.destroy(findProfile.logoId);
          }

          const uploadLogo = await cloudinary.v2.uploader.upload(
            req.files.logo.path,
            cloudinaryOptions
          );
          profileData.logo = uploadLogo.url;
          profileData.logoId = uploadLogo.public_id;
        } catch (err) {
          errors.message =
            'Server error, cannot upload image to cloudinary and save it to database';
          return res.status(500).json(errors);
        }
      }

      try {
        // upsert tells mongoose if it is not created yet then dont update, just create a new one

        let createOrUpdateProfile;

        if (findProfile) {
          createOrUpdateProfile = await Profile.findOneAndUpdate(
            {},
            { $set: profileData },
            { new: true }
          );
        } else {
          createOrUpdateProfile = await new Profile(profileData).save();
        }

        return res.json(createOrUpdateProfile);
      } catch (err) {
        errors.message = 'Server error, cannot create or update profile';
        return res.status(500).json(errors);
      }
    } else {
      errors.message = `You are not authorised to change business's profile`;
      return res.status(401).json(errors);
    }
  }
);

module.exports = router;
