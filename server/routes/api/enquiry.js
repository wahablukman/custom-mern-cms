const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');

// load required models
const Enquiry = require('../../models/Enquiry');

// @route   GET api/enquiry
// @desc    Get all owner's enquiries (only admin allowed)
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    let options = {
      limit: 20,
      sort: req.query.sort || '-createdAt',
      page: parseInt(req.query.page) || 1
    };

    if (req.user.role === 'Admin') {
      try {
        const findEnquiries = await Enquiry.paginate({}, options);
        return res.json(findEnquiries);
      } catch (err) {
        errors.message =
          'Server error, cannot retrieve your enquiries right now';
        return res.status(500).json(errors);
      }
    } else {
      errors.message = 'You are not authorised to get the enquiries';
      return res.status(401).json(errors);
    }
  }
);

// @route   GET api/enquiry/:enquiry_id
// @desc    Get selected owner's enquiry (only admin allowed)
// @access  Private
router.get(
  '/:enquiry_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    if (req.user.role === 'Admin') {
      try {
        const findEnquiry = await Enquiry.findOne({
          _id: req.params.enquiry_id
        });
        return res.json(findEnquiry);
      } catch (err) {
        errors.message = 'Enquiry not found';
        return res.status(404).json(errors);
      }
    } else {
      errors.message = 'You are not authorised to get the enquiries';
      return res.status(401).json(errors);
    }
  }
);

// @route   POST api/enquiry
// @desc    Sending enquiries (does not need to be authorised users)
// @access  Public
router.post('/', async (req, res) => {
  let errors = {};

  const enquiryData = new Enquiry({
    name: req.body.name,
    email: req.body.email,
    phone: req.body.phone,
    message: req.body.message
  });

  try {
    const saveEnquiry = await enquiryData.save();
    return res.json(saveEnquiry);
  } catch (err) {
    errors.message = 'Server error, cannot send your enquiry at this time';
    return res.status(500).json(errors);
  }
});

module.exports = router;
