const mongoose = require('mongoose');
const passport = require('passport');
const express = require('express');
const router = express.Router();
const isEmpty = require('../../validation/isEmpty.validation');

// load models required
const About = require('../../models/About');

// @route   GET api/about/test
// @desc    test posts route
// @access  Public
router.get('/test', (req, res) => {
  res.json({ msg: 'about work' });
});

// @route   GET api/about
// @desc    Get about
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};
  try {
    const about = await About.findOne();
    return res.json(about);
  } catch (err) {
    errors.message = 'server error, cannot get about right now';
    return res.status(404).json(errors);
  }
});

// @route   POST api/about
// @desc    Create or update about
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const aboutContent = new About({
      about: req.body.about
    });
    const aboutExists = await About.findOne();
    //if about exists, overwrite it, if not, create a new one
    if (!isEmpty(aboutExists)) {
      try {
        const updatedAbout = await About.findOneAndUpdate(
          {},
          { about: req.body.about },
          { new: true }
        );
        return res.json(updatedAbout);
      } catch (err) {
        errors.message = 'server error, cannot update your about right now';
        return res.status(500).json(errors);
      }
    } else {
      try {
        const newAbout = await aboutContent.save();
        return res.json(newAbout);
      } catch (err) {
        errors.message = 'server error, cannot create your about right now';
        return res.status(500).json(errors);
      }
    }
  }
);

module.exports = router;
