const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');
const cloudinary = require('cloudinary');
require('../../config/cloudinary');

// load required models
const Testimony = require('../../models/Testimony');

// @route   GET api/testimony
// @desc    Get all website's testimonies
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};

  try {
    const findTestimonies = await Testimony.find();
    return res.json(findTestimonies);
  } catch (error) {
    errors.message = 'Server error, cannot retrieve testimonies at the moment';
    return res.status(500).json(errors);
  }
});

// @route   GET api/testimony/:testimony_id
// @desc    Get all website's testimonies
// @access  Public
router.get('/:testimony_id', async (req, res) => {
  let errors = {};

  try {
    const findTestimony = await Testimony.findOne({
      _id: req.params.testimony_id
    });
    return res.json(findTestimony);
  } catch (error) {
    errors.message = 'Cannot find testimony by that ID';
    return res.status(404).json(errors);
  }
});

// @route   POST api/testimony
// @desc    Create a testimony (Admin only)
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const testimonyData = new Testimony({
      name: req.body.name,
      testimony: req.body.testimony
    });

    if (req.user.role === 'Admin') {
      try {
        const cloudinaryOptions = {
          overwrite: true,
          folder: 'testimony'
        };
        const newAvatar = await cloudinary.v2.uploader.upload(
          req.files.avatar.path,
          cloudinaryOptions
        );
        testimonyData.avatar = newAvatar.url;
        testimonyData.avatarId = newAvatar.public_id;
        const saveTestimony = await testimonyData.save();
        return res.json(saveTestimony);
      } catch (error) {
        errors.message = 'Server error, canot create new testimony right now';
        return res.status(500).json(errors);
      }
    } else {
      errors.message = 'You are not authorised to create new testimony';
      return res.status(401).json(errors);
    }
  }
);

// @route   PUT api/testimony/:testimony_id
// @desc    update a testimony (Admin only)
// @access  Private
router.put(
  '/:testimony_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    if (req.user.role === 'Admin') {
      try {
        const findTestimony = await Testimony.findOne({
          _id: req.params.testimony_id
        });
        if (req.files.avatar) {
          // delete old image and upload the new one if there is new avatar file
          await cloudinary.v2.uploader.destroy(findTestimony.avatarId);
          const cloudinaryOptions = {
            overwrite: true,
            folder: 'testimony'
          };
          const newAvatar = await cloudinary.v2.uploader.upload(
            req.files.avatar.path,
            cloudinaryOptions
          );
          findTestimony.avatar = newAvatar.url;
          findTestimony.avatarId = newAvatar.public_id;
        }
        findTestimony.name = req.body.name;
        findTestimony.testimony = req.body.testimony;
        const saveTestimony = await findTestimony.save();
        return res.json(saveTestimony);
      } catch (error) {
        errors.message = 'cannot find testimony to be updated';
        return res.status(404).json(errors);
      }
    } else {
      errors.message = 'You are not authorised to create new testimony';
      return res.status(401).json(errors);
    }
  }
);

module.exports = router;
