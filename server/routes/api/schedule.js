const express = require('express');
const router = express.Router();
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');

// load required models
const Schedule = require('../../models/Schedule');

// @route   GET api/schedule
// @desc    Get all owner's schedule
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};
  try {
    const findAllSchedule = await Schedule.find();
    return res.json(findAllSchedule);
  } catch (err) {
    errors.message = 'Server error, cannot return any schedule at this time';
    return res.status(500).json(errors);
  }
});

// @route   POST api/schedule
// @desc    Create a schedule
// @access  Private, only Admin allowed
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const scheduleData = new Schedule({
      from: req.body.from,
      title: req.body.title,
      to: req.body.to,
      description: req.body.description
    });

    //only admin allowed to create
    if (req.user.role !== 'Admin') {
      errors.message = 'Only admin allowed to create a schedule';
      return res.status(401).json(errors);
    }

    try {
      const saveSchedule = await scheduleData.save();
      return res.json(saveSchedule);
    } catch (err) {
      errors.message = 'Server error, cannot save the schedule right now';
      return res.status(500).json(errors);
    }
  }
);

// @route   PUT api/schedule/:schedule_id
// @desc    Edit a schedule
// @access  Private, only Admin allowed
router.put(
  '/:schedule_id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};
    const scheduleData = {
      from: req.body.from,
      title: req.body.title,
      to: req.body.to,
      description: req.body.description
    };

    //only admin allowed to edit
    if (req.user.role !== 'Admin') {
      errors.message = 'Only admin allowed to edit schedule';
      return res.status(401).json(errors);
    }

    try {
      const findSchedule = await Schedule.findOne({
        _id: req.params.schedule_id
      });
      if (!isEmpty(findSchedule)) {
        findSchedule.from = scheduleData.from;
        findSchedule.title = scheduleData.title;
        findSchedule.to = scheduleData.to;
        findSchedule.description = scheduleData.description;
        const updateSchedule = await findSchedule.save();
        return res.json(updateSchedule);
      } else {
        errors.message = 'Schedule not found, cannot edit any schedule';
        return res.status(404).json(errors);
      }
    } catch (err) {
      errors.message = 'Server error, cannot edit your schedule at the moment';
      return res.status(500).json(errors);
    }
  }
);

module.exports = router;
