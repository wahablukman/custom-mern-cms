const express = require('express');
const router = express.Router();
const passport = require('passport');
const mongoose = require('mongoose');
const isEmpty = require('../../validation/isEmpty.validation');

// load required models
const SocialMedia = require('../../models/SocialMedia');

// @route   GET api/social-media/test
// @desc    test posts route
// @access  Public
router.get('/test', (req, res) => {
  res.json({ msg: 'bisa nih socmed' });
});

// @route   GET api/social-media
// @desc    Get all social media
// @access  Public
router.get('/', async (req, res) => {
  let errors = {};
  try {
    const socialMedia = await SocialMedia.findOne();
    return res.json(socialMedia);
  } catch (err) {
    errors.message =
      'Server error cannot get social media at the moment please try again later';
    return res.status(500).json(err);
  }
});

// @route   POST api/social-media
// @desc    create / update social media
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const socialMediarequest = {
      instagram: req.body.instagram,
      youtube: req.body.youtube,
      twitter: req.body.twitter,
      facebook: req.body.facebook
    };

    const socialMedia = await SocialMedia.findOne();

    //if social media is not created yet then create one, otherwise update
    if (!isEmpty(socialMedia)) {
      try {
        const updatedSocialMedia = await SocialMedia.findOneAndUpdate(
          {},
          socialMediarequest,
          { new: true }
        );
        return res.json(updatedSocialMedia);
      } catch (err) {
        errors.message = 'Server error, cannot update social media';
        return res.status(500).json(errors);
      }
    } else {
      try {
        const newSocialMedia = new SocialMedia(socialMediarequest);
        const savedSocialMedia = await newSocialMedia.save();
        return res.json(savedSocialMedia);
      } catch (err) {
        errors.message = 'Server error, cannot update social media';
        return res.status(500).json(err);
      }
    }
  }
);

module.exports = router;
