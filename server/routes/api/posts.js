const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const isEmpty = require('../../validation/isEmpty.validation');
const keys = require('../../config/keys');
const cloudinary = require('cloudinary');

require('../../config/cloudinary');

// load models required
const Post = require('../../models/Post');

// load validation required
const validatePostInput = require('../../validation/posts.validation');

// @route   GET api/posts/test
// @desc    test posts route
// @access  Public
router.get('/test', (req, res) => {
  res.json({ msg: 'posts work' });
});

// @route   GET api/posts/all
// @desc    Get all posts
// @access  Public
router.get('/all', async (req, res) => {
  let errors = {};

  // option & query for posts data
  let options = {
    limit: 10,
    sort: req.query.sort || '-createdAt',
    populate: 'user',
    page: parseInt(req.query.page) || 1
  };

  try {
    //get all posts
    const posts = await Post.paginate({}, options);
    return res.json(posts);
  } catch (err) {
    errors.message = 'Server error cannot get posts, please try again in a bit';
    return res.status(500).json(errors);
  }
});

// @route   GET api/posts/all-user
// @desc    Get all posts of a user
// @access  Public
router.get('/all-user/:user_id', async (req, res) => {
  let errors = {};

  // option & query for posts data
  let options = {
    limit: 10,
    sort: req.query.sort || '-createdAt',
    populate: 'user',
    page: parseInt(req.query.page) || 1
  };

  try {
    //get all posts
    const posts = await Post.paginate({ user: req.params.user_id }, options);
    return res.json(posts);
  } catch (err) {
    errors.message = 'Server error cannot get posts, please try again in a bit';
    return res.status(500).json(errors);
  }
});

// @route   POST api/posts
// @desc    Posting a post
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let { errors, isValid } = validatePostInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    // split categories if more than 1
    let categories;
    if (!isEmpty(req.body.categories) || req.body.categories.length != 0) {
      categories = req.body.categories.split(',').map(category => {
        return category.trim();
      });
    } else {
      categories = [];
    }

    //declare new post data
    const { title, content } = req.body;
    const slug = title.replace(/[\W_]+/g, '-').toLowerCase();
    const newPost = new Post({
      user: req.user.id,
      author: req.user.name,
      title,
      normalized: req.body.title.toUpperCase(),
      slug,
      content,
      categories
    });

    //check if featured image request exists
    if (isEmpty(req.files.image)) {
      errors.image = 'Feature image is required';
      return res.status(400).json(errors);
    } else {
      try {
        const cloudinaryOptions = {
          overwrite: true,
          folder: 'posts'
        };
        const featuredImage = await cloudinary.v2.uploader.upload(
          req.files.image.path,
          cloudinaryOptions
        );
        newPost.image = featuredImage.url;
        newPost.imageID = featuredImage.public_id;
      } catch (err) {
        errors.message = 'server error';
        return res.status(500).json(errors);
      }
    }

    //check if the slug exists
    try {
      const existingSlug = await Post.findOne({ slug });
      if (!isEmpty(existingSlug)) {
        // set a new slug
        const newSlug = `${slug}-${new Date().valueOf()}`;
        newPost.slug = newSlug;
      }
    } catch (err) {
      errors.message = 'Server errror cannot update slug';
      return res.status(500).json(errors);
    }

    //save the new post
    try {
      const savePost = await newPost.save();
      return res.json(savePost);
    } catch (err) {
      errors.message = 'Server error cannot save the post now, try again later';
      return res.status(500).json(errors);
    }
  }
);

// @route   POST api/posts/:id
// @desc    Edit a post
// @access  Private
router.post(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    const { errors, isValid } = validatePostInput(req.body);

    if (!isValid) {
      return res.status(400).json(errors);
    }

    //set categories and split them
    let categories;
    if (!isEmpty(req.body.categories) || req.body.categories.length != 0) {
      categories = req.body.categories.split(',').map(category => {
        return category.trim();
      });
    } else {
      categories = [];
    }

    //find the post that needs to be updated
    const existingPost = await Post.findById(req.params.id);
    // if this post exists lets update
    if (!isEmpty(existingPost)) {
      try {
        //if there is a new featured image
        if (!isEmpty(req.files.image)) {
          try {
            const cloudinaryOptions = {
              overwrite: true,
              folder: 'posts'
            };
            // delete old image and upload the new one
            await cloudinary.v2.uploader.destroy(existingPost.imageID);
            const newImage = await cloudinary.v2.uploader.upload(
              req.files.image.path,
              cloudinaryOptions
            );
            existingPost.image = newImage.url;
            existingPost.imageID = newImage.public_id;
          } catch (err) {
            errors.message =
              'Server error, cannot upload your image, try again in a bit';
            return res.status(500).json(errors);
          }
        }
        // update other post data
        // existingPost.user = req.user.id;
        existingPost.author = req.user.name;
        existingPost.title = req.body.title;
        existingPost.normalized = req.body.title.toUpperCase();
        existingPost.content = req.body.content;
        existingPost.categories = categories;

        const updatedPost = await existingPost.save();
        return res.json(updatedPost);
      } catch (err) {
        errors.message = 'Server error, cannot update your post';
        return res.status(500).json(errors);
      }
    } else {
      errors.post = 'There is no post found to be edited';
      return res.status(404).json(error);
    }
  }
);

// @route   GET api/posts/:id
// @desc    Get a post by post ID
// @access  Public
router.get('/:id', async (req, res) => {
  let errors = {};

  // find post by id
  const post = await Post.findById({ _id: req.params.id }).populate('user', [
    'name',
    'email',
    'avatar',
    'role'
  ]);

  //check if a post can be found
  if (!isEmpty(post)) {
    try {
      return res.json(post);
    } catch (err) {
      errors.message = 'Server cannot, cannot get the post right now';
      return res.status(500).json(err);
    }
  } else {
    errors.post = 'A post not found';
    return res.status(404).json(errors);
  }
});

// @route   DELETE api/posts/:id
// @desc    Delete a post by post ID
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let errors = {};

    const post = await Post.findById(req.params.id);

    //check if the post can be found by id
    if (!isEmpty(post)) {
      try {
        // check if post's author and logged in user is the same
        if (req.user.id.toString() === post.user._id.toString()) {
          if (await !isEmpty(post.imageID)) {
            await cloudinary.v2.uploader.destroy(post.imageID);
          }
          await post.remove();
          return res.json({ message: 'delete success' });
        } else {
          errors.post = 'You are not authorised to delete this post';
          return res.status(401).json(errors);
        }
      } catch (err) {
        errors.message = 'server error cannot delete your post';
        return res.status(500).json(errors);
      }
    } else {
      errors.post = 'post not found';
      return res.status(404).json(errors);
    }
  }
);

// @route   GET api/posts/slug/:slug
// @desc    Get a post by slug
// @access  Public
router.get('/slug/:slug', async (req, res) => {
  let errors = {};

  const post = await Post.findOne({ slug: req.params.slug }).populate('user', [
    'name',
    'email',
    'avatar'
  ]);

  // check if the post can be found
  if (!isEmpty(post)) {
    try {
      return res.json(post);
    } catch (err) {
      errors.message =
        'Cannot find your post at the moment, please try again later';
      return res.status(500).json(errors);
    }
  } else {
    errors.post = 'post not found';
    return res.status(404).json(errors);
  }
});

module.exports = router;
