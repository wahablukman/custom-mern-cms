const Validator = require('validator');
const isEmpty = require('./isEmpty.validation');

module.exports = validatePostInput = data => {
  let errors = {};

  // trim all spaces if empty
  data.title = !isEmpty(data.title) ? data.title : '';
  data.content = !isEmpty(data.content) ? data.content : '';

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Title is required';
  }

  if (Validator.isEmpty(data.content)) {
    errors.content = 'Content is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
