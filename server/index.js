const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const port = process.env.PORT || 5000;
const passport = require('passport');
const multipart = require('connect-multiparty');

//routes
const auth = require('./routes/api/auth');
const posts = require('./routes/api/posts');
const about = require('./routes/api/about');
const socialMedia = require('./routes/api/socialMedia');
const category = require('./routes/api/category');
const mainProfile = require('./routes/api/mainProfile');
const schedule = require('./routes/api/schedule');
const enquiry = require('./routes/api/enquiry');
const testimony = require('./routes/api/testimony');

//body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(multipart());

//set static files folder
// if (process.env.NODE_ENV === 'production') {
//   // Set a static folder
//   app.use(express.static('client/build'));
//   app.get('*', (req, res) =>
//     res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
//   );
// }

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to mongoDB
mongoose
  .connect(
    db,
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log('mongodb connected');
    // console.log(res);
  })
  .catch(err => {
    console.log(err);
  });

// passport middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

// declaring routes for easier use
app.use('/api/auth', auth);
app.use('/api/posts', posts);
app.use('/api/about', about);
app.use('/api/social-media', socialMedia);
app.use('/api/category', category);
app.use('/api/main-profile', mainProfile);
app.use('/api/schedule', schedule);
app.use('/api/enquiry', enquiry);
app.use('/api/testimony', testimony);

//starting development server
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
